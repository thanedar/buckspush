<?php
if(!defined('NOTIFICATION_ROOT'))
	define('NOTIFICATION_ROOT', dirname(dirname(__FILE__)));

require_once NOTIFICATION_ROOT . '/utilities/config.inc.php';
require_once NOTIFICATION_ROOT . '/utilities/functions.inc.php';

require_once NOTIFICATION_ROOT . '/utilities/logger.class.php';
require_once NOTIFICATION_ROOT . '/utilities/dbconn.class.php';

require_once NOTIFICATION_ROOT . '/classes/campaign.class.php';

/***
 * Class to handle notification campaign messages
 * Sudhanshu Thanedar
 */

Class BP_Message
{
	private $_environment = 'beta';
	private $_log;
	private $_dbconn;

	private $iCmpgnId;

	/**
	 * Constructor
	 * @return void
	 **/
	public function __construct($iCmpgnId = 0)
	{
		$this->iCmpgnId = $iCmpgnId;

		try {
			$this->_environment = NOTIFY_ENVIRONMENT;
			$this->_log = new Logger();
			$this->_dbconn = new DbConn();
			$this->_dbconn->connectNotifDb();
		}
		catch(PDOException $e) {
			die("Could not initiate the logging and DB connections" . $e->getMessage());
		}
	}

	/* Function to check if campaign already exists
	 * @return boolean
	 */
	public function checkNotificationCampaign()
	{
		$campaign = new BP_Campaign($this->iCmpgnId);
		return $campaign->checkNotificationCampaign();
	}
	
	/* Function to get all campaign messages
	 * @return mixed
	 */
	public function getAllCampaignMessages()
	{
		$this->_log->debug('Entered ' . __FUNCTION__);
		if(!$this->checkNotificationCampaign()) {
			$this->_log->error("Incorrect campaign ID specified. Value cannot be non-numeric.");
		}
		else {
			try {
				$msgStmt = $this->_dbconn->notif->prepare("SELECT * FROM `tats_config`.`push_messages` INNER JOIN `tats_config`.`languages`
						USING (`language_id`) WHERE `campaign_id` = :campaign_id");
				$msgStmt->execute(array(':campaign_id' => $this->iCmpgnId));
				$msgArr = $msgStmt->fetchAll();
			}
			catch (PDOException $e) {
				$this->_log->error("DB Error Message: " . $e->getMessage());
				$this->_log->error("DB Error Info: " . var_export($this->_dbconn->notif->errorInfo(), true));
			}

			if(!empty($msgArr)) {
				return $msgArr;
			}
		}

		return false;
	}

	/* Function to get campaign message with language name
	 * @param $sLanguageName
	 * @return mixed
	 */
	public function getCmpgnMessageByLangName($sLanguageName = '')
	{
		$this->_log->debug('Entered ' . __FUNCTION__);

		if(!$this->checkNotificationCampaign() && !empty($sLanguageName)) {
			try {
				$msgStmt = $this->_dbconn->notif->prepare("SELECT * FROM `tats_config`.`push_messages` INNER JOIN `tats_config`.`languages`
						USING (`language_id`) WHERE `campaign_id` = :campaign_id AND `language_name` = :language_name");
				$msgStmt->execute(array(':campaign_id' => $this->iCmpgnId, ':language_name' => $sLanguageName));
				$msgInfo = $msgStmt->fetch();
			}
			catch (PDOException $e) {
				$this->_log->error("DB Error Message: " . $e->getMessage());
				$this->_log->error("DB Error Info: " . var_export($this->_dbconn->notif->errorInfo(), true));
			}

			if(!empty($msgInfo)) {
				return $msgInfo;
			}
			else {
				return $this->getDefaultCampaignMessage($iCampaignId);
			}
		}
		else {
			$this->_log->error("Incorrect params specified, check campaign ID and language name submitted.");
		}
		
		return false;
	}

	/* Function to get campaign message with language code
	 * @param $iCampaignId
	 * @param $sLanguageCode
	 * @return mixed
	 */
	public function getCmpgnMessageByLangCode($sLanguageCode = '')
	{
		$this->_log->debug('Entered ' . __FUNCTION__);

		if(!$this->checkNotificationCampaign($this->iCmpgnId) && !empty($sLanguageCode)) {
			try {
				$msgStmt = $this->_dbconn->notif->prepare("SELECT * FROM `tats_config`.`push_messages` INNER JOIN `tats_config`.`languages`
						USING (`language_id`) WHERE `campaign_id` = :campaign_id AND `language_code` = :language_code");
				$msgStmt->execute(array(':campaign_id' => $iCampaignId, ':language_code' => $sLanguageCode));
				$msgInfo = $msgStmt->fetch();
			}
			catch (PDOException $e) {
				$this->_log->error("DB Error Message: " . $e->getMessage());
				$this->_log->error("DB Error Info: " . var_export($this->_dbconn->notif->errorInfo(), true));
			}

			if(!empty($msgInfo)) {
				return $msgInfo;
			}
			else {
				return $this->getDefaultCampaignMessage($iCampaignId);
			}
		}
		else {
			$this->_log->error("Incorrect params specified, check campaign ID and language code submitted.");
		}
		
		return false;
	}

	/* Function to get campaign message with language ID
	* @param $iLanguageId
	* @return mixed
	*/
	public function getCmpgnMessageByLangId($iLanguageId = 0)
	{
		$this->_log->debug('Entered ' . __FUNCTION__);

		if(!$this->checkNotificationCampaign() && !empty($iLanguageId)) {
			try {
				$msgStmt = $this->_dbconn->notif->prepare("SELECT * FROM `tats_config`.`push_messages` INNER JOIN `tats_config`.`languages`
						USING (`language_id`) WHERE `campaign_id` = :campaign_id AND `language_id` = :language_id");
				$msgStmt->execute(array(':campaign_id' => $iCampaignId, ':language_id' => $iLanguageId));
				$msgInfo = $msgStmt->fetch();
			}
			catch (PDOException $e) {
				$this->_log->error("DB Error Message: " . $e->getMessage());
				$this->_log->error("DB Error Info: " . var_export($this->_dbconn->notif->errorInfo(), true));
			}

			if(!empty($msgInfo)) {
				return $msgInfo;
			}
			else {
				return $this->getDefaultCampaignMessage();
			}
		}
		else {
			$this->_log->error("Incorrect params specified, check campaign ID and language code submitted.");
		}
		
		return false;
	}

	/* Function to get campaign message with default language
	 * @return mixed
	 */
	public function getDefaultCampaignMessage()
	{
		$this->_log->debug('Entered ' . __FUNCTION__);

		$campaign = new BP_Campaign($this->iCmpgnId);
		$aCmpgnData = $campaign->getNotificationCampaign();

		if(is_array($aCmpgnData)) {
			try {
				$msgStmt = $this->_dbconn->notif->prepare("SELECT * FROM `tats_config`.`push_messages`
						WHERE `campaign_id` = :campaign_id AND `language_id` = :language_id");
				$msgStmt->execute(array(':campaign_id' => $iCampaignId, ':language_id' => $aCmpgnData['default_language']));
				$msgInfo = $msgStmt->fetchAll();
			}
			catch (PDOException $e) {
				$this->_log->error("DB Error Message: " . $e->getMessage());
				$this->_log->error("DB Error Info: " . var_export($this->_dbconn->notif->errorInfo(), true));
			}

			if(!empty($msgInfo)) {
				return $msgInfo;
			}
			else {
				$this->_log->error("No default message found for campaign, check campaign ID submitted.");
			}
		}
		else {
			$this->_log->error("Incorrect params specified - no valid campaign found, check given campaign Id.");
		}
		
		return false;
	}

	/* Function to add new message
	 * @param $aInput array
	 * @return mixed
	 */
	public function addCampaignMessage($aInput)
	{
		$this->_log->debug('Entered ' . __FUNCTION__);
		
		if(is_array($aInput)) {
			$aInput['datetime'] = date("Y-m-d H:i:s");

			$aMsgFields = array('campaign_id', 'message_text', 'sound', 'badge', 'delay_idle', 'collapse', 'time_to_live', 'language_id', 'datetime');
			$aMsgIns = setInsertStrings($aMsgFields, $aInput, true);

			if(is_array($aMsgIns)) {
				try {
					$msgStmt = $this->_dbconn->notif->prepare("INSERT INTO `tats_config`.`push_messages` (" . $aMsgIns['keys'] . ")
							VALUES (" . $aMsgIns['values'] . ")");
					$msgRes = $msgStmt->execute($aInput);
				}
				catch (PDOException $e) {
					$this->_log->error("DB Error Message: " . $e->getMessage());
					$this->_log->error("DB Error Info: " . var_export($this->_dbconn->notif->errorInfo(), true));
				}

				$imsgId = $this->_dbconn->notif->lastInsertId();

				if($msgRes) {
					return $imsgId;
				}
				else {
					$this->_log->error("App config cannot be created.");
				}
			}
			else {
				$this->_log->error("Incorrect config request, table keys not found.");
			}
		}
		else {
			$this->_log->error("Incorrect config request, given data not an array.");
		}
		
		return false;
	}

	/* Function to update messages
	 * @param $aInput array
	 * @return boolean
	 */
	public function updateCampaignMessage($aInput)
	{
		$this->_log->debug('Entered ' . __FUNCTION__);
		
		if(is_array($aInput)) {
			if($this->checkNotificationCampaign()) {
				$aInput['datetime'] = date("Y-m-d H:i:s");

				$aMsgFields = array('message_id', 'campaign_id', 'message_text', 'sound', 'badge', 'delay_idle', 'collapse', 'time_to_live', 'language_id');

				foreach($aMsgFields as $field) {
					if(isset($aInput[$field])) {
						$updateStmt .= "`" . $field . "` = :" . $field . ", ";
					}
				}

				try {
					$msgStmt = $this->_dbconn->notif->prepare("UPDATE `tats_config`.`push_messages` SET ". $updateStmt . " WHERE `message_id` = :message_id");
					$bMsgRes = $msgStmt->execute($aInput);
				}
				catch (PDOException $e) {
					$this->_log->error("DB Error Message: " . $e->getMessage());
					$this->_log->error("DB Error Info: " . var_export($this->_dbconn->notif->errorInfo(), true));
				}

				if($bMsgRes) {
					return true;
				}
				else {
					$this->_log->error("Message update failed.");
				}
			}
			else {
				$this->_log->error("Message cannot be updated, invalid campaign ID given.");
			}
		}
		else {
			$this->_log->error("Incorrect config request, given data not an array.");
		}

		return false;
	}
}
