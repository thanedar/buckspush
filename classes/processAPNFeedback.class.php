<?php

require_once dirname(__FILE__) . '/notification.class.php';
require_once dirname(__FILE__) . '/apns.class.php';

require_once NOTIFICATION_ROOT . '/external/MTDaemon.class.php';

class ProcessHistory extends MTDaemon
{
	private $_dbconn;
	private $_logger;
    /*
     * Optional: read in config files or perform other "init" actions
     * Called from MTDaemon's _prerun(), plus when a SIGHUP signal is received.
     * NOTE: Runs under the PARENT process.
     */

	public function loadConfig()
	{
		$this->_dbconn = new DbConn();
		$this->_dbconn->connectRedis();
		
		$this->_logger = new Logger();
	}

	/*
	* Function to return quickly with (a) no work to do, or (b) next otem to process
	* NOTE: Runs under the PARENT process
	*/
	public function getNext($slot) 
	{
		//$this->_logger->info('Starting ' . __FUNCTION__);
		$pid = file_get_contents($this->sPIDFileName);
		exec("ps -ef| awk '\$3 == '" . $pid . "' { print  \$2 }'", $output, $ret);
		if (count($output) > $this->max_threads) {
			sleep(60);
		}

		try {
			$cnfgStmt = $this->_dbconn->notif->prepare("SELECT * FROM `tats_config`.`mobile_app` WHERE `ios_feedback_datetime` <= DATE_SUB(NOW(), INTERVAL 1 DAY) AND `ios_certificate_file` IS NOT NULL LIMIT 1;");
			$cnfgStmt->execute();
			$cnfgRow = $cnfgStmt->fetch();
		}
		catch (PDOException $e) {
			$this->_log->error("Config select query Error: " . $e->getMessage());
			$this->_log->error("DB Error Info: " . var_export($this->_dbconn->notif->errorInfo(), true));
		}

		if(!empty($cnfgRow)) {
			try {
				$cnfgUpStmt = $this->_dbconn->notif->prepare("UPDATE `tats_config`.`mobile_app` SET `ios_feedback_datetime` = NOW() WHERE `id` = :id");
				$cnfgUpStmt->bindParam(':id', $cnfgRow['id'], PDO::PARAM_INT);
				$cnfgUpStmt->execute();
			}
			catch (PDOException $e) {
				$this->_log->error("Config update query Error: " . $e->getMessage());
				$this->_log->error("DB Error Info: " . var_export($this->_dbconn->notif->errorInfo(), true));
			}
			$this->_logger->info('Config row obtained ' . var_export($cnfgRow, true));
		}	

		return $cnfgRow;
	}

	/*
	* Do main work here.
	* NOTE: Runs under a CHILD process
	*/
	public function run($aCnfgData, $slot)
	{
		$this->_logger->info('Starting ' . __FUNCTION__);
		$this->_logger->info('Received input ' . var_export($aCnfgData, true));
		
		$oApnsNotify = new ApnsNotify();
		$oApnsNotify->setApnsConfig($aCnfgData);
		
		$aReturn = $oApnsNotify->getFeedback();
		if(!empty($aReturn)){
			$oNotify = new BP_Notification();
			foreach($aReturn as $apnFb){
				$oNotify->updateTatsUserApns($aCnfgData, $apnFb);
			}
		}
		
		return 0;
	}
}
