<?php

require_once dirname(__FILE__) . '/appconfig.class.php';
require_once dirname(__FILE__) . '/campaign.class.php';
require_once dirname(__FILE__) . '/notification.class.php';

require_once NOTIFICATION_ROOT . '/external/MTDaemon.class.php';

class ProcessQueue extends MTDaemon
{
	private $_dbconn;
	private $_logger;
	private $_notif;
    /*
     * Optional: read in config files or perform other "init" actions
     * Called from MTDaemon's _prerun(), plus when a SIGHUP signal is received.
     * NOTE: Runs under the PARENT process.
     */

	public function loadConfig()
	{
		$this->_dbconn = new DbConn();
		$this->_dbconn->connectRedis();
		
		$this->_logger = new Logger();
	}

	/*
	* Function to return quickly with (a) no work to do, or (b) next file to process
	* NOTE: Runs under the PARENT process
	* Could keep open DB connection to affinegy_master, and figure out $build here
	*/
	public function getNext($slot) 
	{
		$this->_logger->info('Starting ' . __FUNCTION__);
		$pid = file_get_contents($this->sPIDFileName);
		exec("ps -ef| awk '\$3 == '" . $pid . "' { print  \$2 }'", $output, $ret);
		if (count($output) > $this->max_threads) {
			sleep(1);
		}

		$result = 0;
		try{
			//Fetch the first element that has timestamp less than now
			$row = $this->_dbconn->redis->zrangebyscore(REDIS_QUEUE, -INF, microtime(true), array('limit' => array('count' => 1), 'withscores' => true));
			//$this->_logger->info('row obtained ' . var_export($row, true));
			
			//Check if returned object contains push message
			if (isset($row[0][0]) && (strlen($row[0][0]) > 0)){
				$result = $this->_dbconn->redis->zremrangebyscore(REDIS_QUEUE, $row[0][1], $row[0][1]);
			}
			
			// Return null object if there are no objects or the zrem operation returns 0  
			if (empty($row[0]) || $result === 0) {
				return null;
			}
		}
		catch (Exception $e) {
			$this->_logger->error("Could not connect to redis! Error: " . $e->getMessage());
		}

		$this->_logger->info('returning row ');
		return $row[0][0];
	}
	
	/*
	* Do main work here.
	* NOTE: Runs under a CHILD process
	*/
	public function run($sMessageData, $slot)
	{
		$this->_logger->info('Starting ' . __FUNCTION__);
		$this->_logger->info('Received input ' . var_export($sMessageData, true));
		
		$this->_notif = new BP_Notification();
		
		$aMessageData = json_decode($sMessageData, true);
		$this->_logger->info('calling processQueueMessage with ' . var_export($aMessageData, true));
		$aReturn = $this->_notif->processQueueMessage($aMessageData);
		
		$this->_logger->info('processQueueMessage returned ' . var_export($aReturn, true));
		
		$aReturn['history_id'] = $aMessageData['history_id'];
		$aReturn['push_user_id'] = $aMessageData['push_user_id'];
		$aReturn['notification_sent_datetime'] = date('Y-m-d H:i:s');
		
		if($aReturn['canonical_id']) {
			$this->_notif->updateTatsUserGcm($aMessageData['push_user_id'], $aReturn['canonical_id']);
		}
		elseif($aReturn['notification_retry']) {
			$this->_notif->retryMessageQueue($aMessageData);
		}
		
		$this->_dbconn->redis->rpush(REDIS_HISTORY, json_encode($aReturn));
		
		return 0;
	}
}
