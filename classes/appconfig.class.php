<?php
if(!defined('NOTIFICATION_ROOT'))
	define('NOTIFICATION_ROOT', dirname(dirname(__FILE__)));

require_once NOTIFICATION_ROOT . '/utilities/config.inc.php';
require_once NOTIFICATION_ROOT . '/utilities/functions.inc.php';

require_once NOTIFICATION_ROOT . '/utilities/logger.class.php';
require_once NOTIFICATION_ROOT . '/utilities/dbconn.class.php';

/***
 * App Config class
 * Sudhanshu Thanedar
 */

Class BP_AppConfig
{
	private $_environment = 'beta';
	private $_log;
	private $_dbconn;
	
	private $sAppId;

	/**
	 * Constructor
	 * @return void
	 **/
	public function __construct($sAppId = '')
	{
		$this->sAppId = $sAppId;
		
		try {
			$this->_environment = NOTIFY_ENVIRONMENT;
			$this->_log = new Logger();
			$this->_dbconn = new DbConn();
			$this->_dbconn->connectNotifDb();
		}
		catch(PDOException $e) {
			die("Could not initiate the logging and DB connections" . $e->getMessage());
		}
	}

	/* Function to check if app configuration already exists
	 * @return boolean
	 */
	public function checkAppConfig()
	{
		$this->_log->debug('Entered ' . __FUNCTION__);
		
		if($this->sAppId) {
			try {
				$cnfgStmt = $this->_dbconn->notif->prepare("SELECT count(*) AS `count` FROM `tats_config`.`mobile_app` WHERE `appid` = :appid");
				$cnfgStmt->bindParam(':appid', $this->sAppId);
				$cnfgStmt->execute();
				$cnfgCount = $cnfgStmt->fetch();
			}
			catch (PDOException $e) {
				$this->_log->error("DB Error Message: " . $e->getMessage());
				$this->_log->error("DB Error Info: " . var_export($this->_dbconn->notif->errorInfo(), true));
			}

			if($cnfgCount['count'] > 0) {
				return true;
			}
			else {
				$this->_log->error("Config for given app ID not found.");
			}
		}
		else {
			$this->_log->error("Incorrect app ID specified, parameter value cannot be non-numeric.");
		}

		return false;
	}

	/* Function to get the config ID for app
	 * @return mixed
	 */
	public function getAppConfigId()
	{
		$this->_log->debug('Entered ' . __FUNCTION__);
		
		if($this->sAppId) {
			try {
				$cnfgStmt = $this->_dbconn->notif->prepare("SELECT `id` AS `config_id` FROM `tats_config`.`mobile_app` WHERE `appid` = :appid");
				$cnfgStmt->bindParam(':appid', $this->sAppId);
				$cnfgStmt->execute();
				$cnfgId = $cnfgStmt->fetch();
			}
			catch (PDOException $e) {
				$this->_log->error("DB Error Message: " . $e->getMessage());
				$this->_log->error("DB Error Info: " . var_export($this->_dbconn->notif->errorInfo(), true));
			}

			if($cnfgId['config_id'] > 0) {
				return $cnfgId['config_id'];
			}
			else {
				$this->_log->error("Config ID for given app ID not found.");
			}
		}
		else {
			$this->_log->error("Incorrect app ID specified, parameter value cannot be non-numeric.");
		}

		return false;
	}

	/* Function to get all config for given app ID
	 * @param $sAppId string
	 * @return mixed
	 */
	public function getAppConfigByApp()
	{
		$this->_log->debug('Entered ' . __FUNCTION__);
		if($this->sAppId) {
			try {
				$cnfgStmt = $this->_dbconn->notif->prepare("SELECT *, `id` AS `config_id` FROM `tats_config`.`mobile_app` WHERE `appid` = :appid");
				$cnfgStmt->bindParam(':appid', $this->sAppId);
				$cnfgStmt->execute();
				$cnfgData = $cnfgStmt->fetch();
			}
			catch (PDOException $e) {
				$this->_log->error("DB Error Message: " . $e->getMessage());
				$this->_log->error("DB Error Info: " . var_export($this->_dbconn->notif->errorInfo(), true));
			}

			if($cnfgData) {
				return $cnfgData;
			}
			else {
				$this->_log->error("No Config found for given app ID.");
			}
		}
		else {
			$this->_log->error("Incorrect app ID specified, parameter value cannot be non-numeric.");
		}

		return false;
	}

	/* Function to get all config for given config ID
	 * @param $iCnfgId int
	* @return mixed
	*/
	public function getAppConfigByConfig($iCnfgId)
	{
		$this->_log->debug('Entered ' . __FUNCTION__);
		
		if(intval($iCnfgId)) {
			try {
				$cnfgStmt = $this->_dbconn->notif->prepare("SELECT *, `id` AS `config_id` FROM `tats_config`.`mobile_app` WHERE `id` = :id");
				$cnfgStmt->bindParam(':id', $iCnfgId, PDO::PARAM_INT);
				$cnfgStmt->execute();
				$cnfgData = $cnfgStmt->fetch();
			}
			catch (PDOException $e) {
				$this->_log->error("DB Error Message: " . $e->getMessage());
				$this->_log->error("DB Error Info: " . var_export($this->_dbconn->notif->errorInfo(), true));
			}

			if($cnfgData) {
				return $cnfgData;
			}
			else {
				$this->_log->error("No Config found for given config ID.");
			}
		}
		else {
			$this->_log->error("Incorrect config ID specified, parameter value cannot be non-numeric.");
		}

		return false;
	}

	/* Function to add new app configuration (or call update if it already exists)
	 * @param $aInput array
	 * @return mixed
	 */
	public function addAppConfig($aInput)
	{
		$this->_log->debug('Entered ' . __FUNCTION__);
		
		$this->_log->error("Call disabled as part of move to tats config");
		return false;
		
		if(is_array($aInput)) {
			$bCnfgCount = false;

			if($aInput['app_id']) {
				$this->sAppId = $aInput['app_id'];
				$bCnfgCount = $this->checkAppConfig();
			}

			if(!$bCnfgCount) {
				$aInput['datetime'] = date("Y-m-d H:i:s");

				$aCfgFields = array('appid', 'package_name', 'ios_certificate_file', 'ios_certificate_password',
						'gcm_api_code', 'gcm_api_key', 'datetime');
				$aCfgIns = setInsertStrings($aCfgFields, $aInput, true);

				try {
					$cnfgStmt = $this->_dbconn->notif->prepare("INSERT INTO `tats_config`.`mobile_app` (" . $aCfgIns['keys'] . ")
							VALUES (" . $aCfgIns['values'] . ")");
					$cnfgRes = $cnfgStmt->execute($aInput);
				}
				catch (PDOException $e) {
					$this->_log->error("DB Error Message: " . $e->getMessage());
					$this->_log->error("DB Error Info: " . var_export($this->_dbconn->notif->errorInfo(), true));
				}

				$iCnfgId = $this->_dbconn->notif->lastInsertId();
			}
			else {
				$this->_log->error("App ID also exists, use updateConfig call.");
			}

			if($cnfgRes) {
				return $iCnfgId;
			}
			else {
				$this->_log->error("App config cannot be created.");
			}
		}
		else {
			$this->_log->error("Incorrect config request, given data not an array.");
		}
		return false;
	}

	/* Function to update app configuration
	 * @param $aInput array
	 * @return boolean
	 */
	public function updateAppConfig($aInput)
	{
		$this->_log->debug('Entered ' . __FUNCTION__);
		$this->_log->error("Call disabled as part of move to tats config");
		return false;
		
		if(is_array($aInput))
		{
			$bCnfgCount = false;

			if($aInput['app_id']) {
				$this->sAppId = $aInput['app_id'];
				$bCnfgCount = $this->checkAppConfig();
			}

			if($bCnfgCount) {
				$aInput['datetime'] = date("Y-m-d H:i:s");

				$aCnfgFields = array('package_name', 'ios_certificate_file', 'ios_certificate_password', 'gcm_api_id', 'gcm_api_key');
				$updateStmt = setUpdateString($aCnfgFields, $aInput, true);

				try {
					$cnfgStmt = $this->_dbconn->notif->prepare("UPDATE `tats_config`.`mobile_app` SET ". $updateStmt . " WHERE `appid` = :appid");
					$cnfgStmt->bindParam(':appid', $this->sAppId);
					$bCnfgRes = $cnfgStmt->execute($aInput);
				}
				catch (PDOException $e) {
					$this->_log->error("DB Error Message: " . $e->getMessage());
					$this->_log->error("DB Error Info: " . var_export($this->_dbconn->notif->errorInfo(), true));
				}

				if($bCnfgRes) {
					return true;
				}
				else {
					$this->_log->error("App config update failed.");
				}
			}
			else {
				$this->_log->error("App config cannot be updated, invalid app ID given.");
			}
		}
		else {
			$this->_log->error("Incorrect config request, given data not an array.");
		}

		return false;
	}
}
