<?php
/***
 * Class for handling messages to iOS devices through APNS
 *
 */

class ApnsNotify
{
	private $_log;
	private $_dbConn;

	private $sPushCert;
	private $sPushPassphrase;

	private $sAlert = '';
	private $sBadge = '';
	private $sSound = '';

	private $aMessage = '';

	private $iTimeToLive = 2419200;	// Keeping default same as GCM - 28 days

	private $iNotifId = 0;
	private $sTrackingId = '';

	private $sDeviceToken = '';

	private $ctx = '';
	private $sslConn = '';

	private $aStatus = array('0' => 'No errors encountered',
		'1' => 'Processing error',
		'2' => 'Missing device token',
		'3' => 'Missing topic',
		'4' => 'Missing payload',
		'5' => 'Invalid token size',
		'6' => 'Invalid topic size',
		'7' => 'Invalid payload size',
		'8' => 'Invalid token',
		'10' => 'Shutdown',
		'255' => 'None (unknown)',);

	const PAYLOAD_MAXIMUM_SIZE = 256; /**< @type integer The maximum size allowed for a notification payload. */
	const APPLE_RESERVED_NAMESPACE = 'aps'; /**< @type string The Apple-reserved aps namespace. */

	const APNS_DEV_PUSH_SERVER  = 'gateway.sandbox.push.apple.com:2195';
	const APNS_PROD_PUSH_SERVER = 'gateway.push.apple.com:2195';

	const APNS_DEV_FEEDBACK_SERVER  = 'feedback.sandbox.push.apple.com:2196';
	const APNS_PROD_FEEDBACK_SERVER = 'feedback.push.apple.com:2196';

	function __construct(){
		try
		{
			$this->_log = new Logger();
			$this->_dbconn = new DbConn();
		}
		catch(Exception $e)
		{
			die("Could not initiate the logging and DB connections for APNS");
		}
	}

	/* Set config params from array
	 * @param $aCnfgInput - Array
	* @return boolean
	*/
	function setApnsConfig($aCnfgInput)
	{
		if(is_array($aCnfgInput)){
			#$this->_log->debug('Config Input is ' . var_export($aCnfgInput, true));
			$this->sPushCert = '/tmp/' . $aCnfgInput['app_id'] . $aCnfgInput['package_name'] . '.pem';
			file_put_contents($this->sPushCert, base64_decode($aCnfgInput['ios_certificate_file']));
			$this->sPushPassphrase = $aCnfgInput['ios_certificate_passphrase'];
			$this->_log->debug('Local Cert is ' . $this->sPushCert);
			$this->_log->debug('Passphrase is ' . $this->sPushPassphrase);

			return true;
		}
		else{
			$this->_log->error("Incorrect config request, given data not an array.");
		}

		return false;
	}

	/* Set message params from array
	 * @param $aMsgInput - Array
	* @return boolean
	*/
	function setApnsMsg($aMsgInput)
	{
		if(is_array($aMsgInput)){
			#$this->_log->debug('Input Message array is ' . var_export($aMsgInput, true));
			$this->aMessage = json_decode($aMsgInput['message_text'], true);
			$this->_log->debug('Message array is ' . var_export($this->aMessage, true));

			if(is_array($this->aMessage)){
				$this->sAlert = $this->aMessage;
			}
			else{
				$this->sAlert = $aMsgInput['message_text'];
				$this->aMessage = $aMsgInput['message_text'];
			}

			$this->_log->debug('Alert is ' . var_export($this->sAlert, true));

			$this->iNotifId = $aMsgInput['history_id'];
			$this->sTrackingId = $aMsgInput['tracking_id'];

			if(!empty($aMsgInput['badge'])){
				$this->sBadge = $aMsgInput['badge'];
			}

			if(!empty($aMsgInput['sound'])){
				$this->sSound = $aMsgInput['sound'];
			}

			if(intval($aMsgInput['time_to_live'])){
				$this->iTimeToLive = intval($aMsgInput['time_to_live']);
			}

			$this->sDeviceToken = $aMsgInput['device_id'];

			return true;
		}
		else{
			$this->_log->error("Incorrect config request, given data not an array.");
		}

		return false;
	}

	/* Opens an SSL/TLS connection to Apple's Push Feedback Service (APNS)
	 * and gets the tuples
	 * @return boolean TRUE on success, FALSE on failure.
	 */
	function getFeedback()
	{
		$this->ctx = stream_context_create();

		stream_context_set_option($this->ctx, 'ssl', 'local_cert', $this->sPushCert);
		stream_context_set_option($this->ctx, 'ssl', 'passphrase', $this->sPushPassphrase);

		$this->_log->debug('Environment: ' . NOTIFY_ENVIRONMENT);
		if('live' == NOTIFY_ENVIRONMENT)
			$server = ApnsNotify::APNS_PROD_FEEDBACK_SERVER;
		else	
			$server = ApnsNotify::APNS_DEV_FEEDBACK_SERVER;

		$this->_log->debug('Connecting to server ' . $server);
		$this->sslConn = stream_socket_client(
			'ssl://' . $server, $err, $errstr, 10,
			STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $this->ctx);

		if (!$this->sslConn) {
			$this->_log->error("Failed to connect: $err $errstr");
			return FALSE;
		}

		$this->_log->debug('Connection OK');
		
		$tuples = stream_get_contents($this->sslConn);
		while ($data = fread($this->sslConn, 38)) {
			$feedback = unpack("N1timestamp/n1tokenLength/H*deviceToken", $data);
			$aApnsTuples[] = $feedback;
		}
		
		fclose($this->sslConn);
		$this->sslConn = NULL;

		if (empty($aApnsTuples)){
			echo 'Failed to download device tokens' . PHP_EOL;
			return FALSE;
		}
		else{
			return $aApnsTuples;
		}
	}

	/* Opens an SSL/TLS connection to Apple's Push Notification Service (APNS).
	 * @return boolean TRUE on success, FALSE on failure.
	 */
	function connectToGateway()
	{
		if (!$this->sslConn) {
			$this->ctx = stream_context_create();

			$this->_log->debug('Local Cert is ' . $this->sPushCert);
			$this->_log->debug('Passphrase is ' . $this->sPushPassphrase);
			stream_context_set_option($this->ctx, 'ssl', 'local_cert', $this->sPushCert);
			stream_context_set_option($this->ctx, 'ssl', 'passphrase', $this->sPushPassphrase);

			$this->_log->debug('Environment: ' . NOTIFY_ENVIRONMENT);
			if('live' == NOTIFY_ENVIRONMENT)
				$server = ApnsNotify::APNS_PROD_PUSH_SERVER;
			else	
				$server = ApnsNotify::APNS_DEV_PUSH_SERVER;

			$this->_log->debug('Connecting to server ' . $server);
			$this->sslConn = @stream_socket_client(
				'ssl://' . $server, $err, $errstr, 10,
				STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $this->ctx);

			if (!$this->sslConn) {
				$this->_log->error("Failed to connect: $err $errstr");
				return FALSE;
			}
		}
		
		$this->_log->debug('Connection OK');
		return TRUE;
	}

	/* Drops the connection to the APNS server.
	 *
	 */
	function disconnectFromGateway()
	{
		fclose($this->sslConn);
		$this->sslConn = NULL;
	}

	/* Attempts to reconnect to Apple's Push Notification Service. Exits with
	 * an error if the connection cannot be re-established after 3 attempts.
	 */
	function reconnectToGateway()
	{
		$this->disconnectFromGateway();

		$attempt = 1;

		while (true)
		{
			$this->_log->info("Reconnecting to server, attempt $attempt");

			if ($this->connectToGateway())
				return TRUE;

			if ($attempt++ > 3){
				$this->_log->error('Could not reconnect after 3 attempts');
				return FALSE;
			}

			sleep(2);
		}
	}

	/* Push message to APNS servers
	 *
	* @return boolean
	*/
	function sendMessage()
	{
		$aReturn['notification_sent_status'] = 'ER';
		$aReturn['notification_error_details'] = '';

		$aPayload = array();

		$this->_log->debug('Connecting to Gateway');
		$connect = $this->connectToGateway();

		if (strlen($this->sDeviceToken) != 64){
			$this->_log->error("Message " . $this->iNotifId . " has invalid device token");
			$sNotifDetails = 'Notification failed - Message ' . $this->iNotifId . ' has invalid device token';
		}
		else if (!$connect){
			$this->_log->debug('Entered reconnect');
			$reconnect = $this->reconnectToGateway();
		}

		if(!$connect && !$reconnect){
			$sNotifDetails = 'Notification failed - Connection failed';
			$aReturn['notification_retry'] = true;
		}
		else{
			$this->_log->debug('Entered send block');

			// Check each element is set to reduce the size of payload as MAX 256 bytes are allowed
			if(!empty($this->sAlert)){
				$aPayload['aps']['alert'] = $this->sAlert;
			}
			if(!empty($this->sBadge)){
				$aPayload['aps']['badge'] = intval($this->sBadge);
			}
			if(!empty($this->sSound)){
				$aPayload['aps']['sound'] = $this->sSound;
			}

			//$aPayload['message'] = $this->aMessage;
			$aPayload['nid'] = $this->iNotifId;

			if(!empty($this->sTrackingId)){
				$aPayload['tid'] = $this->sTrackingId;
			}

			$sPayload = json_encode($aPayload);
			$this->_log->debug("Sending message " . $this->iNotifId . " to '" . $this->sDeviceToken . "', payload: '" . $sPayload . "'");

			// The ''new'' enhanced notification format
			$frame = chr(1)                                 // device token item ID
			     . pack('n', 32)                            // token length (2 bytes)
			     . pack('H*', $this->sDeviceToken)          // device token (32 bytes)
				 . chr(2)                                   // payload item ID
			     . pack('n', strlen($sPayload))             // payload length (2 bytes)
			     . $sPayload                                // the JSON payload
				 . chr(3)                                   // notification identifier item ID
			     . pack('n', 32)                            // token length (2 bytes)
			     . pack('N', $this->iNotifId)               // identifier (4 bytes)
				 . chr(4)                                   // expiration/TTL item ID
			     . pack('n', 32)                            // token length (2 bytes)
			     . pack('N', time() + $this->iTimeToLive);  // expire after (4 bytes)

			$this->_log->debug('Frame to send is ' . $frame);
			$msg = chr(2)                                   // command (1 byte)
			     . pack('N', strlen($frame))                // frame length (4 bytes)
			     . $frame;                                  // the payload frame created above

			// Change the result eval to check if APNS returned any error message before closing connection
			// And add to REDIS_HISTORY to queue to add to history

			$this->_log->debug('Message to send is ' . $msg);
			$result = fwrite($this->sslConn, $msg, strlen($msg));
			if (!$result){
				$sNotifDetails = 'Notification failed - fwrite returned false';
				$aReturn['notification_retry'] = true;
			}
			else if ($result != strlen($msg)){
				$sNotifDetails = 'Notification failed - fwrite result not equal to strlen(msg), message tried: ' . $msg;
				$aReturn['notification_retry'] = true;
			}

			usleep(10000);
			stream_set_blocking($this->sslConn, FALSE);
			#stream_set_timeout($this->sslConn, 1);
			
			$sRet = '';
			$info = stream_get_meta_data($this->sslConn);
			$this->_log->debug('Info array is ' . var_export($info, true));

			$sRet = stream_get_contents($this->sslConn);

			#while (!feof($this->sslConn)){
			#while (!feof($this->sslConn) && !($info['timed_out'])){
				#$this->_log->debug('Entered while loop ' . var_export($info, true));
				#$sRet = stream_get_contents($this->sslConn);
				#$sRet .= fread($this->sslConn, 6);
				#$info = stream_get_meta_data($this->sslConn);
				#$this->_log->debug(' New Info array is ' . var_export($info, true));
				#usleep(100000);
			#}

			if($sRet){
				$aStRet = unpack('Ccommand/Cstatus_code/Nidentifier', $sRet);
				$sNotifDetails = 'Notification failed - APNS returned status ' . $aStRet['status_code'] . ' - ' . $this->aStatus[$aStRet['status_code']];
			}
			else{
				$aReturn['notification_sent_status'] = 'OK';
				$sNotifDetails = 'Notification sent successfully';
			}
		}

		$aReturn['notification_error_details'] = $sNotifDetails;
		$this->_log->error($sNotifDetails);

		return $aReturn;
	}
}
