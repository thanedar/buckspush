<?php

require_once dirname(__FILE__) . '/notification.class.php';

require_once NOTIFICATION_ROOT . '/external/MTDaemon.class.php';

class ProcessHistory extends MTDaemon
{
	private $_dbconn;
	private $_logger;
    /*
     * Optional: read in config files or perform other "init" actions
     * Called from MTDaemon's _prerun(), plus when a SIGHUP signal is received.
     * NOTE: Runs under the PARENT process.
     */

	public function loadConfig()
	{
		$this->_dbconn = new DbConn();
		$this->_dbconn->connectRedis();
		
		$this->_logger = new Logger();
	}

	/*
	* Function to return quickly with (a) no work to do, or (b) next otem to process
	* NOTE: Runs under the PARENT process
	*/
	public function getNext($slot) 
	{
		//$this->_logger->info('Starting ' . __FUNCTION__);
		$pid = file_get_contents($this->sPIDFileName);
		exec("ps -ef| awk '\$3 == '" . $pid . "' { print  \$2 }'", $output, $ret);
		if (count($output) > $this->max_threads) {
			sleep(1);
		}

		try{
			$row = $this->_dbconn->redis->lpop(REDIS_HISTORY);
			if(!empty($row)) {
				$this->_logger->info('row obtained ' . var_export($row, true));
			}	
		}
		catch (Exception $e) {
			$this->_logger->error("Could not connect to redis! Error: " . $e->getMessage());
		}

		return $row;
	}
	
	/*
	* Do main work here.
	* NOTE: Runs under a CHILD process
	*/
	public function run($sMessageData, $slot)
	{
		$this->_logger->info('Starting ' . __FUNCTION__);
		$this->_logger->info('Received input ' . var_export($sMessageData, true));
		
		$aMessageData = json_decode($sMessageData, true);
		$this->_dbconn->connectNotifDb();

		try {
			$updateStmt = $this->_dbconn->notif->prepare("UPDATE `history` 
				SET `notification_sent_datetime` = :notification_sent_datetime, `notification_sent_status` = :notification_sent_status, `notification_error_details` = :notification_error_details 
				WHERE `history_id` = :history_id ");
			
			$updateStmt->bindValue(':notification_sent_datetime', $aMessageData['notification_sent_datetime']);
			$updateStmt->bindValue(':notification_sent_status', $aMessageData['notification_sent_status']);
			$updateStmt->bindValue(':notification_error_details', $aMessageData['notification_error_details']);
			$updateStmt->bindValue(':history_id', $aMessageData['history_id'], PDO::PARAM_INT);
			$result = $updateStmt->execute();
			
			//$this->_logger->info('DB update returned ' . var_export($result, true));
		} 
		catch (PDOException $e) {
			$this->_logger->error("DB Error Message: " . $e->getMessage());
			$this->_logger->error("DB Error Info: " . var_export($this->_dbconn->notif->errorInfo(), true));
		}
		
		return 0;
	}
}
