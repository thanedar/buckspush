<?php
if(!defined('NOTIFICATION_ROOT'))
	define('NOTIFICATION_ROOT', dirname(dirname(__FILE__)));

require_once NOTIFICATION_ROOT . '/utilities/config.inc.php';
require_once NOTIFICATION_ROOT . '/utilities/functions.inc.php';

require_once NOTIFICATION_ROOT . '/utilities/logger.class.php';
require_once NOTIFICATION_ROOT . '/utilities/dbconn.class.php';

require_once NOTIFICATION_ROOT . '/external/predis.client.php';

require_once NOTIFICATION_ROOT . '/classes/gcm.class.php';
require_once NOTIFICATION_ROOT . '/classes/apns.class.php';

/***
 * Main Notification class to handle sending mobile device notifications
 * Sudhanshu Thanedar
 */

Class BP_Notification
{
	private $_environment = 'beta';
	private $_log;
	private $_dbconn;
	
	private $_oGcmNotify = null;
	private $_oApnsNotify = null;

	/**
	 * Constructor
	 * @return void
	 **/
	public function __construct()
	{
		try {
			$this->_environment = NOTIFY_ENVIRONMENT;
			$this->_log = new Logger();
			$this->_dbconn = new DbConn();
			$this->_dbconn->connectNotifDb();
		}
		catch(PDOException $e) {
			die("Could not initiate the logging and DB connections" . $e->getMessage());
		}
	}

	/* Function to add new messages from campaigns to queue
	 * @param $aInput array with message information
	 *
	 */
	public function queueCampaignMessage($aInput)
	{
		$this->_log->debug('Entered ' . __FUNCTION__);
		
		if(is_array($aInput)) {
			// get campaign info
			$oCmpgn = new BP_Campaign($aInput['campaign_id']);
			
			$iCmpgnId = $aInput['campaign_id'];
			$aCmpgnData = $oCmpgn->getNotificationCampaign();
			//$this->_log->debug("Campaign data is " . var_export($aCmpgnData, true));

			// get message for the given campaign
			$aMsgData = $oCmpgn->getCmpgnMessageByLangName($aInput['language']);
			//$this->_log->debug("Message data is " . var_export($aMsgData, true));

			// get scheduled time depending on user timezone
			$sMsgSchTime = $oCmpgn->getScheduleTime($aInput['timezone']);

			$aCmpgnMsgFields = array('push_user_id', 'campaign_id', 'config_id', 'message_id', 'message_text', 'sound', 'badge', 'delay_idle', 'collapse', 'time_to_live',
					'device_type', 'device_id', 'tracking_id', 'notification_insert_datetime', 'notification_schedule_datetime', 'notification_sent_status', 'history_date_id');

			$aCmpgnMsgIns = setInsertStrings($aCmpgnMsgFields, $aInput, false);

			try {
				$queueStmt = $this->_dbconn->notif->prepare("INSERT INTO `history` (" . $aCmpgnMsgIns['keys'] . ")
						VALUES (" . $aCmpgnMsgIns['values'] . ")");

				$queueStmt->bindValue(':push_user_id', $aInput['push_user_id']);
				$queueStmt->bindParam(':campaign_id', $iCmpgnId, PDO::PARAM_INT);
				$queueStmt->bindParam(':config_id', $aCmpgnData['config_id'], PDO::PARAM_INT);

				// Message parameters - some for Android and some for iOS
				$queueStmt->bindParam(':message_id', $aMsgData['message_id'], PDO::PARAM_INT);
				$queueStmt->bindParam(':message_text', $aMsgData['message_text']);
				$queueStmt->bindParam(':sound', $aMsgData['sound']);
				$queueStmt->bindParam(':badge', $aMsgData['badge']);
				$queueStmt->bindParam(':delay_idle', $aMsgData['delay_idle']);
				$queueStmt->bindParam(':collapse', $aMsgData['collapse']);
				$queueStmt->bindParam(':time_to_live', $aMsgData['time_to_live']);

				$queueStmt->bindParam(':device_type', $aInput['device_type']);
				$queueStmt->bindParam(':device_id', $aInput['device_id']);

				$queueStmt->bindValue(':tracking_id', $aInput['tracking_id'] ? $aInput['tracking_id'] : '');
				$queueStmt->bindValue(':notification_insert_datetime', date('Y-m-d H:i:s'));
				$queueStmt->bindValue(':notification_schedule_datetime', $sMsgSchTime);
				$queueStmt->bindValue(':notification_sent_status', 'WT');
				$queueStmt->bindValue(':history_date_id', date('YmdH'), PDO::PARAM_INT);
				$queueStmt->execute();
			}
			catch (PDOException $e) {
				$this->_log->error("DB Error Message: " . $e->getMessage());
				$this->_log->error("DB Error Info: " . var_export($this->_dbconn->notif->errorInfo(), true));
			}

			// Get history ID to add to Redis Queue data
			$iHistoryId = $this->_dbconn->notif->lastInsertId();

			$aCmpgnMsgData['push_user_id'] = $aInput['push_user_id'];
			$aCmpgnMsgData['campaign_id'] = $iCmpgnId;
			$aCmpgnMsgData['config_id'] = $aCmpgnData['config_id'];
			$aCmpgnMsgData['history_id'] = $iHistoryId;

			// Message parameters - some for Android and some for iOS
			$aCmpgnMsgData['message_id'] = $aMsgData['message_id'];
			$aCmpgnMsgData['message_text'] = $aMsgData['message_text'];

			$aCmpgnMsgData['sound'] = $aMsgData['sound'];
			$aCmpgnMsgData['badge'] = $aMsgData['badge'];
			$aCmpgnMsgData['delay_idle'] = $aMsgData['delay_idle'];
			$aCmpgnMsgData['collapse'] = $aMsgData['collapse'];
			$aCmpgnMsgData['time_to_live'] = $aMsgData['time_to_live'];

			$aCmpgnMsgData['device_type'] = $aInput['device_type'];
			$aCmpgnMsgData['device_id'] = $aInput['device_id'];
			$aCmpgnMsgData['tracking_id'] = $aInput['tracking_id'] ? $aInput['tracking_id'] : '';

			$aCmpgnMsgData['notification_insert_datetime'] = date('Y-m-d H:i:s');
			$aCmpgnMsgData['notification_schedule_datetime'] = $sMsgSchTime;
			$aCmpgnMsgData['notification_sent_status'] = 'WT';

			$this->redisZAdd(strtotime($sMsgSchTime), json_encode($aCmpgnMsgData));
			return true;
		}
		else {
			$this->_log->error("Incorrect request submitted, given data is not formatted as an array.");
			return false;
		}
	}

	/* Function to add new custom messages to queue
	 * @param $aInput array with message information
	*
	*/
	public function queueCustomMessage($aInput)
	{
		$this->_log->debug('Entered ' . __FUNCTION__);
		
		if(is_array($aInput)) {
			$this->_log->debug("Input: " . var_export($aInput, true));

			// get app config info
			$oAppConfig = new BP_AppConfig($aInput['app_id']);
			$aCnfgData = $oAppConfig->getAppConfigByApp();

			$aCstMsgFields = array('push_user_id', 'product_user_id', 'config_id', 'message_text', 'sound', 'badge', 'delay_idle', 'collapse', 'time_to_live',
					'device_type', 'device_id', 'tracking_id', 'notification_insert_datetime', 'notification_schedule_datetime', 'notification_sent_status', 'history_date_id');

			$aCstMsgIns = setInsertStrings($aCstMsgFields, $aInput, false);
			$this->_log->debug("Insert Str : " . var_export($aCstMsgIns, true));
			$sDatetime = date('Y-m-d H:i:s');
			$this->_log->debug("Insert datetime : " . var_export($sDatetime, true));

			try {
				$queueStmt = $this->_dbconn->notif->prepare("INSERT INTO `history` (" . $aCstMsgIns['keys'] . ")
						VALUES (" . $aCstMsgIns['values'] . ")");

				$queueStmt->bindValue(':push_user_id', $aInput['push_user_id']);
				$queueStmt->bindValue(':product_user_id', $aInput['product_user_id'], PDO::PARAM_INT);
				$queueStmt->bindValue(':config_id', $aCnfgData['config_id'], PDO::PARAM_INT);

				$queueStmt->bindValue(':message_text', !empty($aInput['message_text']) ? $aInput['message_text'] : $aInput['message']);
				$queueStmt->bindParam(':sound', $aInput['sound']);
				$queueStmt->bindParam(':badge', $aInput['badge']);
				$queueStmt->bindParam(':delay_idle', $aInput['delay_idle']);
				$queueStmt->bindParam(':collapse', $aInput['collapse']);
				$queueStmt->bindParam(':time_to_live', $aInput['time_to_live']);

				$queueStmt->bindValue(':device_type', $aInput['device_type'] ? $aInput['device_type'] : '');
				$queueStmt->bindValue(':device_id', $aInput['device_id'] ? $aInput['device_id'] : '');
				$queueStmt->bindValue(':tracking_id', $aInput['tracking_id'] ? $aInput['tracking_id'] : '');

				$queueStmt->bindValue(':notification_insert_datetime', $sDatetime);
				$queueStmt->bindValue(':notification_schedule_datetime', $sDatetime);
				$queueStmt->bindValue(':notification_sent_status', 'WT');
				$queueStmt->bindValue(':history_date_id', date('YmdH'), PDO::PARAM_INT);
				$queueStmt->execute();
			}
			catch (PDOException $e) {
				$this->_log->error("DB Error Message: " . $e->getMessage());
				$this->_log->error("DB Error Info: " . var_export($this->_dbconn->notif->errorInfo(), true));
			}

			// Get history ID to add to Redis Queue data
			$iHistoryId = $this->_dbconn->notif->lastInsertId();

			$aCstMsgData['push_user_id'] = $aInput['push_user_id'];
			$aCstMsgData['config_id'] = $aCnfgData['config_id'];
			$aCstMsgData['history_id'] = $iHistoryId;

			$aCstMsgData['message_text'] = !empty($aInput['message_text']) ? $aInput['message_text'] : $aInput['message'];
			$aCstMsgData['sound'] = $aInput['sound'];
			$aCstMsgData['badge'] = $aInput['badge'];
			$aCstMsgData['delay_idle'] = $aInput['delay_idle'];
			$aCstMsgData['collapse'] = $aInput['collapse'];
			$aCstMsgData['time_to_live'] = $aInput['time_to_live'];

			$aCstMsgData['device_type'] = $aInput['device_type'];
			$aCstMsgData['device_id'] = $aInput['device_id'];
			$aCstMsgData['tracking_id'] = $aInput['tracking_id'] ? $aInput['tracking_id'] : '';

			$aCstMsgData['notification_insert_datetime'] = $sDatetime;
			$aCstMsgData['notification_schedule_datetime'] = $sDatetime;
			$aCstMsgData['notification_sent_status'] = 'WT';

			$this->redisZAdd(time(), json_encode($aCstMsgData));
			return true;
		}
		else {
			$this->_log->error("Incorrect request submitted, given data is not formatted as an array.");
			return false;
		}
	}

	/* Function to retry message by putting it back in queue
	 * @param $aMessageData array
	 */
	public function retryMessageQueue($aMessageData)
	{
		$this->_log->debug('Entered ' . __FUNCTION__);
		
		if($aMessageData['retry_count']) {
			$aMessageData['retry_count'] += 1;
		}
		else {
			$aMessageData['retry_count'] = 1;
		}

		if($aMessageData['retry_count'] < 3) {
			$this->_log->info('Retried message with score ' . strtotime('+1 hour') . ' and data' . json_encode($aMessageData));
			$this->redisZAdd( strtotime('+1 hour'), json_encode($aMessageData));
			return true;
		}
		return false;
	}

	/* Function to send messages in queue to the users
	 * @param $aInput array with message information
	 * @return $aReturn array
	 */
	public function processQueueMessage($aInput)
	{
		$this->_log->debug('Entered ' . __FUNCTION__);
		$aReturn = array('notification_sent_status' => 'ER', 'notification_sent_error' => '');
		
		$oAppConfig = new BP_AppConfig();
		$aCnfgData = $oAppConfig->getAppConfigByConfig($aInput['config_id']);

		if($aCnfgData) {
			if($aInput['device_type'] == 'Android') {
				// send message using GCM class
				
				if(!is_object($this->_oGcmNotify)){
					$this->_oGcmNotify = new GcmNotify();
				}

				$this->_oGcmNotify->setGcmConfig($aCnfgData);
				$this->_oGcmNotify->setGcmMsg($aInput);

				$aReturn = $this->_oGcmNotify->sendMessage();
			}
			else if($aInput['device_type'] == 'iOS') {
				// send message using APNS class
				
				if(!is_object($this->_oGcmNotify)){
					$this->_oApnsNotify = new ApnsNotify();
				}

				$this->_oApnsNotify->setApnsConfig($aCnfgData);
				$this->_oApnsNotify->setApnsMsg($aInput);

				$aReturn = $this->_oApnsNotify->sendMessage();
			}
			else {
				$this->_log->error("Incorrect device type specified. Skipped device ID " . $aInput['device_id']);
				$aReturn['notification_sent_error'] = "Incorrect device type specified. Skipped device ID " . $aInput['device_id'];
			}
		}
		else {
			$this->_log->error("Incorrect app config specified. Skipped device ID " . $aInput['device_id']);
			$aReturn['notification_sent_error'] = "Incorrect app config specified. Skipped device ID " . $aInput['device_id'];
		}

		return $aReturn;
	}

	/* Function to get schedule time for user given timezone
	 * @param $iCampaignId
	 * @param $sTimezone
	 * @return mixed
	 */
	private function getScheduleTime($iCampaignId, $sTimezone)
	{
		$this->_log->debug('Entered ' . __FUNCTION__);
		
		$oCmpgn = new BP_Campaign($iCampaignId);
		$aCmpgnData = $oCmpgn->getNotificationCampaign();
		
		if(is_array($aCmpgnData)) {
			$curLocal = new DateTime();
			
			if(!empty($sTimezone)) {
				$curRemote = new DateTime(NULL, new DateTimeZone($sTimezone));
				$curHour = $curRemote->format('H');

				if($aCmpgnData['start_hour']) {
					if($aCmpgnData['start_hour'] <= $curHour)
					{
						if($aCmpgnData['end_hour']) {
							if($aCmpgnData['end_hour'] >= $curHour) {
								return $curLocal->format('Y-m-d H:i:s');
							}
							else {
								// start time for next day
								return __getModifiedTime($sTimezone, false, $aCmpgnData['start_hour']);
							}
						}
						else {
							return $curLocal->format('Y-m-d H:i:s');
						}
					}
					else {
						// start time for today
						return __getModifiedTime($sTimezone, true, $aCmpgnData['start_hour']);
					}
				}
				else if($aCmpgnData['end_hour']) {
					if($aCmpgnData['end_hour'] >= $curHour) {
						return $curLocal->format('Y-m-d H:i:s');
					}
					else {
						// start time for next day
						return __getModifiedTime($sTimezone, false);
					}
				}
				else {
					return $curLocal->format('Y-m-d H:i:s');
				}
			}
			else {
				$this->_log->debug("No time-zone given, using current time");
				return $curLocal->format('Y-m-d H:i:s');
			}
		}
		else {
			$this->_log->error("Incorrect params specified - no valid campaign found, check given campaign Id.");
		}

		return false;
	}

	/* Function to get modified schedule time for user as current time is not in given range
	 * @param $sTimezone
	 * @param $bToday
	 * @param $sStart
	 * @return $sSchDT
	 */
	public function __getModifiedTime($sTimezone, $bToday = true, $sStart = '0')
	{
		$this->_log->debug('Entered ' . __FUNCTION__);
		$sDay = $bToday ? 'today' : 'tomorrow';

		$localTZ = new DateTimeZone(date_default_timezone_get());
		$remoteTZ = new DateTimeZone($sTimezone);

		$schMsgPush = new DateTime($sDay, $remoteTZ);

		if($sStart != '0') {
			$schMsgPush->add(new DateInterval('PT'.$sStart.'H'));
		}

		$schMsgPush->setTimezone($localTZ);
		return $schLocal->format('Y-m-d H:i:s');
	}

	/* Function to get user info from TATS db
	 * @param $sPushUserId string
	 * @param $iProdUserId int
	 * @param $sAppId string
	 * @param $iCmpgnId int
	 * @return boolean
	 */
	public function getTatsUserInfo($sPushUserId, $iProdUserId, $sAppId, $iCmpgnId)
	{
		$this->_log->debug('Entered ' . __FUNCTION__);
		$bValidCnfg = false;
		$bValidCmpgn = false;

		if($sAppId) {
			$oAppConfig = new BP_AppConfig($sAppId);
			$bValidCnfg = $oAppConfig->checkAppConfig();
		}
		else if(intval($iCmpgnId)) {
			$oCmpgn = new BP_Campaign($iCmpgnId);
			$bValidCmpgn = $oCmpgn->checkNotificationCampaign();
			if($bValidCmpgn) {
				$aCmpgnData = $oCmpgn->getNotificationCampaign();
			}
		}

		if(!$bValidCmpgn && !$bValidCnfg) {
			$this->_log->error("Incorrect campaign and app IDs specified, cannot locate any matching data.");
		}
		else {
			$app_id = $sAppId ? $oAppConfig->getAppConfigId() : $aCmpgnData['config_id'];

			$this->_dbconn->connectTatsMasterDb();
			try {
				if($iProdUserId) {
					$tatsStmt = $this->_dbconn->tats->prepare("SELECT * FROM `tats_trans`.`ts_push_user`
						WHERE `id` = :push_user_id AND `m_app_id` = :app_id AND `advertiser_transid` = :advertiser_transid
						ORDER BY `auto_id` DESC LIMIT 1");
					$tatsStmt->bindParam(':advertiser_transid', $iProdUserId, PDO::PARAM_INT);
				}
				else {
					$tatsStmt = $this->_dbconn->tats->prepare("SELECT * FROM `tats_trans`.`ts_push_user`
						WHERE `id` = :push_user_id AND `m_app_id` = :app_id
						ORDER BY `auto_id` DESC LIMIT 1");
				}

				$tatsStmt->bindValue(':app_id', $app_id, PDO::PARAM_INT);
				$tatsStmt->bindValue(':push_user_id', $sPushUserId, PDO::PARAM_STR);
				$tatsStmt->execute();
				$tatsInfo = $tatsStmt->fetch();
				$this->_log->debug('TATS returned ' .var_export($tatsInfo, true));
			}
			catch (PDOException $e) {
				$this->_log->error("DB Error Message: " . $e->getMessage());
				$this->_log->error("DB Error Info: " . var_export($this->_dbconn->tats->errorInfo(), true));
			}

			try {
				$tatsOsStmt = $this->_dbconn->tats->prepare("SELECT `mo`.* FROM `tats_config`.`mobile_app` `ma` JOIN `tats_config`.`mobile_os` `mo`
					WHERE `ma`.`id` = :app_id AND `ma`.`mobile_os_id` = `mo`.`id` 
					ORDER BY `id` DESC LIMIT 1");

				$tatsOsStmt->bindParam(':app_id', $app_id, PDO::PARAM_INT);
				$tatsOsStmt->execute();
				$tatsOsInfo = $tatsOsStmt->fetch();
				$this->_log->debug('TATS OS call returned ' .var_export($tatsOsInfo, true));
			}
			catch (PDOException $e) {
				$this->_log->error("TATS OS Info DB Error: " . $e->getMessage());
				$this->_log->error("DB Error Info: " . var_export($this->_dbconn->tats->errorInfo(), true));
			}

			if(!empty($tatsInfo) && !empty($tatsOsInfo)) {
				/*  ‘device_type’ : ‘Android’,
					‘device_id’ : ‘Abcd123456ABCD’, // GCM or APNS ID of the device
					‘timezone’ : ‘America/New_York’,
					‘language’ : ‘English’,
				*/
				$aTatsRet['device_type'] = $tatsOsInfo['name'];

				$aTatsRet['device_id'] = $tatsInfo['m_user_notification_id'];
				$aTatsRet['timezone'] = $tatsInfo['m_time_zone'];
				$aTatsRet['language'] = $tatsInfo['m_language'];

				return $aTatsRet;
			}
			else {
				$this->_log->error("No user info found for given ID in TATS, check the given user ID.");
			}

		}

		return false;
	}

	/* Function to update user notification id in TATS db for GCM canonical update
	 * @param $sPushUserId string
	 * @param $sNotificationId string
	 * @return boolean
	 */
	public function updateTatsUserGcm($sPushUserId, $sNotificationId)
	{
		$this->_log->debug('Entered ' . __FUNCTION__);
		$this->_dbconn->connectTatsMasterDb();

		try {
			$srcStmt = $this->_dbconn->tats->prepare("SELECT `id` FROM `tats_config`.`push_user_source` WHERE `name` = 'google notification'");
			$srcStmt->execute();
			$srcArr = $srcStmt->fetch();
		}
		catch (PDOException $e) {
			$this->_log->error("DB Error Message: " . $e->getMessage());
			$this->_log->error("DB Error Info: " . var_export($this->_dbconn->tats->errorInfo(), true));
		}
		$gcmsource = $srcArr['id'];

		try {
			$tatsStmt = $this->_dbconn->tats->prepare("UPDATE `tats_trans`.`ts_push_user`
				SET `m_user_notification_id` = :notification_id, `source_id` = :source_id
				WHERE `id` = :push_user_id ");

			$tatsStmt->bindParam(':push_user_id', $sPushUserId);
			$tatsStmt->bindParam(':notification_id', $sNotificationId);
			$tatsStmt->bindParam(':source_id', $gcmsource, PDO::PARAM_INT);
			$result = $tatsStmt->execute();

			return $result;
		}
		catch (PDOException $e) {
			$this->_log->error("DB Error Message: " . $e->getMessage());
			$this->_log->error("DB Error Info: " . var_export($this->_dbconn->tats->errorInfo(), true));
			return false;
		}
	}

	/* Function to update user notification id in TATS db
	 * @param $aCnfgData array of app config
	 * @param $aApnFb array with APNS feedback
	 * @return boolean
	 */
	public function updateTatsUserApns($aCnfgData, $aApnFb)
	{
		$this->_log->debug('Entered ' . __FUNCTION__);
		$this->_dbconn->connectTatsMasterDb();

		try {
			$srcStmt = $this->_dbconn->tats->prepare("SELECT `id` FROM `tats_config`.`push_user_source` WHERE `name` = 'apple notification'");
			$srcStmt->execute();
			$srcArr = $srcStmt->fetch();
		}
		catch (PDOException $e) {
			$this->_log->error("Source ID Query Error: " . $e->getMessage());
			$this->_log->error("DB Error Info: " . var_export($this->_dbconn->tats->errorInfo(), true));
		}
		$apnsource = $srcArr['id'];

		try {
			$pushUserStmt = $this->_dbconn->tats->prepare("SELECT `updatedatetime` FROM `tats_trans`.`ts_push_user` WHERE `m_user_notification_id` = :notification_id");
			$tatsStmt->bindParam(':notification_id', $aApnFb['deviceToken']);
			$pushUserStmt->execute();
			$userArr = $pushUserStmt->fetch();
		}
		catch (PDOException $e) {
			$this->_log->error("Push User Info Query Error: " . $e->getMessage());
			$this->_log->error("DB Error Info: " . var_export($this->_dbconn->tats->errorInfo(), true));
		}
		$userUpdated = strtotime($userArr['updatedatetime']);

		if($userUpdated < $aApnFb['timestamp']){
			try {
				$tatsUpStmt = $this->_dbconn->tats->prepare("UPDATE `tats_trans`.`ts_push_user`
					SET `deregistered` = :deregistered, `source_id` = :source_id
					WHERE `m_user_notification_id` = :notification_id ");

				$tatsUpStmt->bindValue(':deregistered', 1, PDO::PARAM_INT);
				$tatsUpStmt->bindValue(':source_id', $apnsource, PDO::PARAM_INT);
				$tatsUpStmt->bindParam(':notification_id', $aApnFb['deviceToken']);
				$result = $tatsUpStmt->execute();

				return $result;
			}
			catch (PDOException $e) {
				$this->_log->error("Push User Update Query Error: " . $e->getMessage());
				$this->_log->error("DB Error Info: " . var_export($this->_dbconn->tats->errorInfo(), true));
				return false;
			}
		}
		else{
			$this->_log->debug('Given user updated after APNS timestamp ' . var_export($aApnFb, true));
		}
	}

	/* Function to add messages to redis set
	 * @param $iScore int
	 * @param $sData string
	 * @return boolean
	 */
	public function redisZAdd($iScore, $sData)
	{
		$this->_log->debug('Entered ' . __FUNCTION__);
		$this->_dbconn->connectRedis();

		// Adding randomization in score value to avoid multiple messages with same exact score
		$fInsScore = $iScore + (mt_rand(1, 99999) / 1000000);

		$this->_dbconn->redis->zadd(REDIS_QUEUE, $fInsScore, $sData);
		$this->_log->info('Queued message with score ' . $fInsScore . ' in queue ' . REDIS_QUEUE . ' and data' . $sData);

		return true;
	}

	public function testRedisConn()
	{
		$this->_log->debug('Entered ' . __FUNCTION__);

		$this->redisZAdd(time(), 'test message for redis');

		$this->_dbconn->redis->rpush(REDIS_HISTORY, json_encode(array('history_id' => 10, 'sent_time'=> time(), 'sent_status' => 'OK', 'error'=> 'None')));
		$this->_log->debug('Listed message in list ' . REDIS_HISTORY, json_encode(array('history_id' => 10, 'sent_time'=> time(), 'sent_status' => 'OK', 'error'=> 'None')));
		return true;
	}

	/* Function to check message history
	 * @param $push_user_id string
	 * @param $tracking_id string
	 * @param $history_id int
	 * @return string
	 */
	public function getNotificationHistory($push_user_id, $tracking_id, $history_id)
	{
		$this->_log->debug('Entered ' . __FUNCTION__);
		try {
			if(!empty($history_id)) {
				$histQuery = "SELECT `history_id` AS `id`, `message_text` AS `Message`, `device_type` AS `Type`, `tracking_id`, 
					`notification_insert_datetime` AS `Inserted`, `notification_sent_datetime` AS `Sent`, 
					`notification_sent_status` AS `Send_Status`, `notification_error_details` AS `Error_Details` 
					FROM `history` AS h WHERE `push_user_id` = :push_user_id AND `history_id` = :history_id LIMIT 1;";
	
				$histStmt = $this->_dbconn->notif->prepare($histQuery);
				$histStmt->bindValue(':push_user_id', $push_user_id);
				$histStmt->bindValue(':history_id', $history_id);
			}
			else if(!empty($tracking_id)) {
				$histQuery = "SELECT `history_id` AS `id`, `message_text` AS `Message`, `device_type` AS `Type`, `tracking_id`, 
					`notification_insert_datetime` AS `Inserted`, `notification_sent_datetime` AS `Sent`, 
					`notification_sent_status` AS `Send_Status`, `notification_error_details` AS `Error_Details` 
					FROM `history` AS h WHERE `push_user_id` = :push_user_id AND `tracking_id` = :tracking_id LIMIT 1;";
	
				$histStmt = $this->_dbconn->notif->prepare($histQuery);
				$histStmt->bindValue(':push_user_id', $push_user_id);
				$histStmt->bindValue(':tracking_id', $tracking_id);
			}
			else {
				$histQuery = "SELECT `history_id` AS `id`, `message_text` AS `Message`, `device_type` AS `Type`, `tracking_id`, 
					`notification_insert_datetime` AS `Inserted`, `notification_sent_datetime` AS `Sent`, 
					`notification_sent_status` AS `Send_Status`, `notification_error_details` AS `Error_Details` 
					FROM `history` AS h WHERE `push_user_id` = :push_user_id ORDER BY `history_id` DESC LIMIT 20;";
	
				$histStmt = $this->_dbconn->notif->prepare($histQuery);
				$histStmt->bindValue(':push_user_id', $push_user_id);
			}

			$histStmt->execute();
			$histData = $histStmt->fetchAll();
		}
		catch (PDOException $e) {
			$this->_log->error("DB Error Message: " . $e->getMessage());
			$this->_log->error("DB Error Info: " . var_export($this->_dbconn->notif->errorInfo(), true));
		}

		if($histData) {
			return $histData;
		}
		else {
			$this->_log->error("No data found for given IDs.");
			return false;
		}
	}
}
