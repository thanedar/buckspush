<?php
if(!defined('NOTIFICATION_ROOT'))
	define('NOTIFICATION_ROOT', dirname(dirname(__FILE__)));

require_once NOTIFICATION_ROOT . '/utilities/config.inc.php';
require_once NOTIFICATION_ROOT . '/utilities/functions.inc.php';

require_once NOTIFICATION_ROOT . '/utilities/logger.class.php';
require_once NOTIFICATION_ROOT . '/utilities/dbconn.class.php';

/***
 * Class to handle notification campaigns
 * Sudhanshu Thanedar
 */

Class BP_Campaign
{
	private $_environment = 'beta';
	private $_log;
	private $_dbconn;

	private $iCmpgnId;

	/**
	 * Constructor
	 * @return void
	 **/
	public function __construct($iCmpgnId = 0)
	{
		$this->iCmpgnId = intval($iCmpgnId);

		try {
			$this->_environment = NOTIFY_ENVIRONMENT;
			$this->_log = new Logger();
			$this->_dbconn = new DbConn();
			$this->_dbconn->connectNotifDb();
		}
		catch(PDOException $e) {
			die("Could not initiate the logging and DB connections" . $e->getMessage());
		}
	}

	/* Function to check if campaign already exists
	 * @return boolean
	 */
	public function checkNotificationCampaign()
	{
		$this->_log->debug('Entered ' . __FUNCTION__);
		
		if(intval($this->iCmpgnId) == 0) {
			$this->_log->error("Incorrect campaign ID specified. Value cannot be non-numeric.");
		}
		else {
			try {
				$cmpgnStmt = $this->_dbconn->notif->prepare("SELECT count(*) AS `count` 
					FROM `tats_config`.`push_campaign` `pc` INNER JOIN `tats_config`.`mobile_app` `ma`
					WHERE `pc`.`config_id` = `ma`.`id` AND `campaign_id` = :campaignId");
				$cmpgnStmt->execute(array('campaignId' => $this->iCmpgnId));
				$cmpgnCount = $cmpgnStmt->fetch();
			}
			catch (PDOException $e) {
				$this->_log->error("DB Error Message: " . $e->getMessage());
				$this->_log->error("DB Error Info: " . var_export($this->_dbconn->notif->errorInfo(), true));
			}

			if($cmpgnCount['count'] > 0) {
				return true;
			}
			else {
				$this->_log->error("Campaign ID cannot be found.");
			}
		}

		return false;
	}

	/* Function to get campaign information data
	 * @return mixed
	 */
	public function getNotificationCampaign()
	{
		$this->_log->debug('Entered ' . __FUNCTION__);

		if(intval($this->iCmpgnId) == 0) {
			$this->_log->error("Incorrect campaign ID specified. Value cannot be non-numeric.");
		}
		else {
			try {
				$cmpgnStmt = $this->_dbconn->notif->prepare("SELECT * FROM `tats_config`.`push_campaign` WHERE `campaign_id` = :campaign_id");
				$cmpgnStmt->execute(array(':campaign_id' => $this->iCmpgnId));
				$cmpgnInfo = $cmpgnStmt->fetch();
			}
			catch(PDOException $e){
				$this->_log->error("DB Error Message: " . $e->getMessage());
				$this->_log->error("DB Error Info: " . var_export($this->_dbconn->notif->errorInfo(), true));
			}

			return $cmpgnInfo;
		}

		return false;
	}

	/* Function to get all campaign languages
	* @return mixed
	*/
	public function getAllCampaignLanguages()
	{
		$this->_log->debug('Entered ' . __FUNCTION__);
		try {
			$langStmt = $this->_dbconn->notif->prepare("SELECT * FROM `languages`");
			$langArr = $langStmt->fetchAll();
		}
		catch (PDOException $e) {
			$this->_log->error("DB Error Message: " . $e->getMessage());
			$this->_log->error("DB Error Info: " . var_export($this->_dbconn->notif->errorInfo(), true));
		}

		if(!empty($langArr)) {
			return $langArr;
		}

		return false;
	}

	/* Function to get schedule time for user given timezone
	 * @param $sTimezone
	 * @return mixed
	 */
	private function getScheduleTime($sTimezone)
	{
		$this->_log->debug('Entered ' . __FUNCTION__);

		$aCmpgnData = $this->getNotificationCampaign();

		if(is_array($aCmpgnData)) {
		
			$curLocal = new DateTime();
			if(!empty($sTimezone)) {
				$curRemote = new DateTime(NULL, new DateTimeZone($sTimezone));
				$curHour = $curRemote->format('H');

				if($aCmpgnData['start_hour']) {
					if($aCmpgnData['start_hour'] <= $curHour) {
						if($aCmpgnData['end_hour']) {
							if($aCmpgnData['end_hour'] >= $curHour) {
								return $curLocal->format('Y-m-d H:i:s');
							}
							else {
								// start time for next day
								return __getModifiedTime($sTimezone, false, $aCmpgnData['start_hour']);
							}
						}
						else {
							return $curLocal->format('Y-m-d H:i:s');
						}
					}
					else {
						// start time for today
						return __getModifiedTime($sTimezone, true, $aCmpgnData['start_hour']);
					}
				}
				else if($aCmpgnData['end_hour']) {
					if($aCmpgnData['end_hour'] >= $curHour) {
						return $curLocal->format('Y-m-d H:i:s');
					}
					else {
						// start time for next day
						return __getModifiedTime($sTimezone, false);
					}
				}
				else {
					return $curLocal->format('Y-m-d H:i:s');
				}
			}
			else {
				$this->_log->debug("No time-zone given, using current time");
				return $curLocal->format('Y-m-d H:i:s');
			}
		}
		else {
			$this->_log->error("Incorrect params specified - no valid campaign found, check given campaign Id.");
		}

		return false;
	}

	/* Function to get modified schedule time for user as current time is not in given range
	 * @param $sTimezone
	 * @param $bToday
	 * @param $sStart
	 * @return $sSchDT
	 */
	public function __getModifiedTime($sTimezone, $bToday = true, $sStart = '0')
	{
		$this->_log->debug('Entered ' . __FUNCTION__);
		$sDay = $bToday ? 'today' : 'tomorrow';

		$localTZ = new DateTimeZone(date_default_timezone_get());
		$remoteTZ = new DateTimeZone($sTimezone);

		$schMsgPush = new DateTime($sDay, $remoteTZ);

		if($sStart != '0'){
			$schMsgPush->add(new DateInterval('PT'.$sStart.'H'));
		}

		$schMsgPush->setTimezone($localTZ);
		return $schLocal->format('Y-m-d H:i:s');
	}

	/* Function to add new campaign
	 * @param $aInput array
	 * @return boolean
	 */
	public function addNotificationCampaign($aInput)
	{
		$this->_log->debug('Entered ' . __FUNCTION__);
		$bCnfgCount = false;
		
		if(is_array($aInput) && $aInput['app_id'])
		{
			$oAppConfig = new AppConfig($aInput['app_id']);
			$bCnfgCount = $oAppConfig->checkAppConfig();
			
			if($bCnfgCount){
				$iCnfgId = $oAppConfig->getAppConfigId();

				$aCmpgnFields = array('config_id', 'event_id', 'default_language', 'device_types',
						'start_datetime', 'end_datetime', 'start_hour', 'end_hour', 'campaign_notes', 'active', 'datetime');
				$aCmpgnIns = setInsertStrings($aCmpgnFields, $aInput, true);

				try {
					$cmpgnStmt = $this->_dbconn->notif->prepare("INSERT INTO `tats_config`.`push_campaign` (" . $aCmpgnIns['keys'] . ")
							VALUES (" . $aCfgIns['values'] . ")");
					$cmpgnStmt->execute($aInput);
				}
				catch (PDOException $e) {
					$this->_log->error("DB Error Message: " . $e->getMessage());
					$this->_log->error("DB Error Info: " . var_export($this->_dbconn->notif->errorInfo(), true));
				}
			}
		}
		else {
			$this->_log->error("Incorrect ID specified or given request  is not an array.");
		}
		return false;
	}

	/* Function to update campaign
	 * @param $aInput array
	 * @return boolean
	 */
	public function updateNotificationCampaign($aInput)
	{
		$this->_log->debug('Entered ' . __FUNCTION__);
		
		if(is_array($aInput)) {
			$bCmpgnCount = false;

			if(intval($aInput['campaign_id'])) {
				$this->iCmpgnId = $aInput['campaign_id'];
				$bCmpgnCount = $this->checkNotificationCampaign();
			}

			if($bCmpgnCount) {
				$aInput['datetime'] = date("Y-m-d H:i:s");

				$aCmpgnFields = array('config_id', 'event_id', 'default_language', 'device_types',
						'start_datetime', 'end_datetime', 'start_hour', 'end_hour', 'campaign_notes', 'active');
				$updateStmt = setUpdateString($aCmpgnFields, $aInput, true);

				try {
					$cmpgnStmt = $this->_dbconn->notif->prepare("UPDATE `tats_config`.`push_campaign` SET ". $updateStmt . "
							WHERE `campaign_id` = :campaign_id");
					$bCmpgnRes = $cmpgnStmt->execute($aInput);
				}
				catch (PDOException $e) {
					$this->_log->error("DB Error Message: " . $e->getMessage());
					$this->_log->error("DB Error Info: " . var_export($this->_dbconn->notif->errorInfo(), true));
				}

				if($bCmpgnRes) {
					return true;
				}
				else {
					$this->_log->error("Campaign update failed.");
				}
			}
			else {
				$this->_log->error("Campaign cannot be updated, invalid campaign ID given.");
			}
		}
		else {
			$this->_log->error("Incorrect campaign request, given data not an array.");
		}

		return false;
	}
}
