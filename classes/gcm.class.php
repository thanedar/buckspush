<?php
/*** 
 * Class for handling messages to Android devices through GCM
 *
 */
 
class GcmNotify
{
	private $_log;
	private $_dbConn;
	
	private $sApiCode;
	private $sApiKey;
	
	private $sMessageText = '';
	private $sCollapse = '';
	private $bDelayIdle = FALSE;
	private $iTimeToLive = 2419200;	// GCM Default set at four weeks (in seconds) -> 60 * 60 * 24 * 28
	
	private $iNotifId = 0;
	private $sTrackingId = '';
	
	private $aDeviceIds = array();
	
	const URL_PUSH_NOTIFICATION = 'https://android.googleapis.com/gcm/send';
	
	function __construct(){
		try
		{
			$this->_log = new Logger();
			$this->_dbconn = new DbConn();
		}
		catch(Exception $e)
		{
			die("Could not initiate the logging and DB connections for GCM");
		}
	}
	
	/* Set config params from array
	 * @param $aCnfgInput - Array
	 * @return boolean 
	 */ 
	function setGcmConfig($aCnfgInput)
	{
		if(is_array($aCnfgInput))
		{
			$this->sApiCode = $aCnfgInput['gcm_api_code'];
			$this->sApiKey = $aCnfgInput['gcm_api_key'];
			
			return true;
		}
		else{
			$this->_log->error("Incorrect config request, given data not an array.");
		}
		
		return false;
	}
	
	/* Set message params from array
	 * @param $aMsgInput - Array
	 * @return boolean 
	 */ 
	function setGcmMsg($aMsgInput)
	{
		if(is_array($aMsgInput))
		{
			$this->sMessageText = json_decode($aMsgInput['message_text'], true);
			if(is_null($this->sMessageText)){
				$this->sMessageText = $aMsgInput['message_text'];
			}
			
			$this->iNotifId = $aMsgInput['history_id'];
			$this->sTrackingId = $aMsgInput['tracking_id'];
			
			$this->sCollapse = $aMsgInput['collapse'];
			$this->bDelayIdle = $aMsgInput['delay_idle'];
			$this->iTimeToLive = $aMsgInput['time_to_live'];
			
			$this->aDeviceIds[] = $aMsgInput['device_id'];

			return true;
		}
		else{
			$this->_log->error("Incorrect config request, given data not an array.");
		}
		
		return false;
	}
	
	/* Push message to GCM servers
	 * 
	 * @return boolean 
	 */ 
	function sendMessage()
	{
		$aFields = array(
			'registration_ids' => $this->aDeviceIds,
			'data' => array('message' => $this->sMessageText, 'notification_id' => $this->iNotifId, 'tracking_id' => $sTrackingId ),
		);
		
		if(!empty($this->sCollapse)){
			$aFields['collapse'] = $this->sCollapse;
		}
		if(!empty($this->bDelayIdle)){
			$aFields['delay_while_idle'] = ($this->bDelayIdle === "true"); // required due to strict boolean check by GCM
		}
		if(!empty($this->iTimeToLive)){
			$aFields['time_to_live'] = intval($this->iTimeToLive);
		}
		
		$aHeaders = array(
			'Authorization: key=' . $this->sApiKey,
			'Content-Type: application/json'
		);

		$this->_log->info('Message JSON ' . json_encode($aFields));
		
		// Open connection
		$ch = curl_init();
		// Set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, self::URL_PUSH_NOTIFICATION);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $aHeaders);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($aFields));
		// Execute post
		$result = curl_exec($ch);
		$info = curl_getinfo($ch);

		$this->_log->info('Info JSON ' . var_export($info, true));
		$this->_log->info('Result JSON ' . var_export($result, true));

		$aStatus['notification_sent_status'] = 'ER';
		$aStatus['notification_error_details'] = '';

		if(empty($result)) {
			$aStatus['notification_error_details'] = 'Cannot send notification - Connection failed';
		}
		else {
			$resObj = json_decode($result);
			$this->_log->info('Result Object ' . var_export($resObj, true));
			
			if($info['http_code'] >= 500) {
				$aStatus['notification_error_details'] = 'Retrying notification, received HTTP CODE ' . $info['http_code'];
				$aStatus['notification_retry'] = true;
			}
			else if($info['http_code'] == 400 || $info['http_code'] == 401) {
				$aStatus['notification_sent_status'] = 'ER';
				$aStatus['notification_error_details'] = $resObj->results[0]->error ? $resObj->results[0]->error : 'HTTP CODE ' . $info['http_code'];
			}
			else {
				$aStatus['notification_error_details'] = 'Multicast ID ' . $resObj->multicast_id;
				if(intval($resObj->success) > 0) {
					$aStatus['notification_sent_status'] = 'OK';
					$aStatus['notification_error_details'] .= ' Message ID ' . $resObj->results[0]->message_id;
					
					if(intval($resObj->canonical_ids) > 0) {
						$aStatus['canonical_id'] = $resObj->results[0]->registration_id;
					}
				}
				else if(intval($resObj->failure) > 0){
					$aStatus['notification_sent_status'] = 'ER';
					$aStatus['notification_error_details'] .= ' GCM Error ' . $resObj->results[0]->error;
				}
			}
		}

		$this->_log->info('Return JSON ' . json_encode($aStatus));
		return $aStatus;
	}
	
}
