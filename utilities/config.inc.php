<?php

error_reporting(E_ERROR | E_WARNING);

if( !empty($_SERVER['SERVER_ADDR']) && preg_match('/10\.200\.4\.10[01]{1}/' , $_SERVER['SERVER_ADDR']) ) {
	define('NOTIFY_ENVIRONMENT', 'live');
}
else if( !empty($_SERVER['HOSTNAME']) && preg_match('/acous..push/' , $_SERVER['HOSTNAME']) ) {
	define('NOTIFY_ENVIRONMENT', 'live');
}
else if( !empty($_SERVER['DOCUMENT_ROOT']) && preg_match('/production/' , $_SERVER['DOCUMENT_ROOT']) ) {
	define('NOTIFY_ENVIRONMENT', 'live');
}
else if( !empty($_SERVER['SERVER_ADDR']) && preg_match('/10\.200\.6\.5/' , $_SERVER['SERVER_ADDR']) ) {
	define('NOTIFY_ENVIRONMENT', 'beta');
}
else if( !empty($_SERVER['HOSTNAME']) && preg_match('/acous...dev/' , $_SERVER['HOSTNAME']) ) {
	define('NOTIFY_ENVIRONMENT', 'beta');
}
else if($_SERVER['SERVER_ADDR'] == '10.5.208.71' || preg_match('/nydevweb2/' , $_SERVER['HOSTNAME'])) {
	define('NOTIFY_ENVIRONMENT', 'beta');
}
else {
	die('Invalid environment, cannot configure system');
}

if( ! ini_get('date.timezone') ){
	date_default_timezone_set('America/New_York');
}

# Queue parameters
define('MAX_THREADS', 5);
define('IDLE_SLEEP_TIME', 15);

# Notification Database config
$betaNotDb = array(
	'dbtype' => 'mysql',
	'server' => '10.200.6.5',
	'dbname' => 'notification',
	'username' => 'tats',
	'password' => 'tats',
);

$liveNotDb = array(
	'dbtype' => 'mysql',
	'server' => '10.200.40.70',
	'dbname' => 'tats_push',
	'username' => 'tats',
	'password' => 'tats',
);
	
# TATS Database config
$masterTatsDb = array(
	'dbtype' => 'mysql',
	'server' => '10.200.40.70',
	'dbname' => 'tats_trans',
	'username' => 'tats',
	'password' => 'tats',
);

$slaveTatsDb = array(
	'dbtype' => 'mysql',
	'server' => '10.200.40.71',
	'dbname' => 'tats_trans',
	'username' => 'tats',
	'password' => 'tats',
);

# REDIS config
define('REDIS_QUEUE', 'bucksnotify');
define('REDIS_HISTORY', 'buckshistory');

$betaRedis = array(
	'server' => '127.0.0.1',
	'database' => 2,
	'port' => 6379,
);

$liveRedis = array(
	'server' => '127.0.0.1',
	'database' => 5,
	'port' => 6379,
);
