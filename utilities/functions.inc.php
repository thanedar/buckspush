<?php

function getInputParameters($array)
{
	global $logger;

	$logger->debug('Input is ' . var_export($array, true));
	if(is_array($array)) {
		if(isset($array['json'])) {
			$logger->debug("JSON request " . var_export(json_decode($array['json'], true), true));
			return json_decode($array['json'], true);
		}
		else if(count($array) > 0) {
			$logger->debug("POST request array " . var_export($array, true));
			return $array;
		}
	}
	else {
		$logger->error("Cannot proceed - Empty/Invalid POST request");
		return false;
	}
}

/* Helper function to create Insert strings
 * @param $aFields array - fields in DB table
 * @param $aInput array  - input params to used in query
 * @param $bCheckVal boolean - flag to set if value is checked
 * @return array
 */
function setInsertStrings($aFields, $aInput, $bCheckVal = true)
{
	$keys = $values = '';
	foreach($aFields as $field)
	{
		if($bCheckVal) {
			if(isset($aInput[$field])) {
				$keys .= "`" . $field . "`, ";
				$values .= ":" . $field . ", ";
			}
		}
		else
		{
			$keys .= "`" . $field . "`, ";
			$values .= ":" . $field . ", ";
		}
	}

	$aInsertFields = array('keys' => substr($keys, 0, -2), 'values' => substr($values, 0, -2));
	return $aInsertFields;
}

/* Helper function to create Update string
 * @param $aFields array - fields in DB table
 * @param $aInput array  - input params to used in query
 * @param $bCheckVal boolean - flag to set if value is checked
 * @return string
 */
function setUpdateString($aFields, $aInput, $bCheckVal = true)
{
	$sUpdateStmt = '';
	foreach($aFields as $field)
	{
		if($bCheckVal) {
			if(isset($aInput[$field])) {
				$sUpdateStmt .= "`" . $field . "` = :" . $field . ", ";
			}
		}
		else
		{
			$sUpdateStmt .= "`" . $field . "` = :" . $field . ", ";
		}
	}

	return substr($sUpdateStmt, 0, -2);
}


