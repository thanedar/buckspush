<?php

require_once dirname(__FILE__) . '/config.inc.php';

class Logger
{
	/**
	 * log_file - the log file to write to
	 * @var string
	 **/
	private $_log_file;

	const LOG_DIR = '/var/www/html/logs/api.buckspush.com/';
	const LOG_FORMAT = 'Ymd';
	const DATE_FORMAT = 'Y-m-d G:i:s';

	/**
	 * Constructor
	 * @return void
	 **/
	function __construct()
	{
		$this->_log_file = self::LOG_DIR . 'logger_' . date(self::LOG_FORMAT) . '.log';
		// echo $this->log_file;
		if(!file_exists($this->_log_file)){ //Attempt to create log file
			touch($this->_log_file);
		}

		//Make sure we have write permissions
		if(!(is_writable($this->_log_file)))
		{
			$pwu_data = posix_getpwuid(posix_geteuid());
			$username = $pwu_data['name'];
			
			$this->_log_file = self::LOG_DIR . 'logger_' . date(self::LOG_FORMAT) . '_' . $username . '.log';
			touch($this->_log_file);
			//Cant write to file,
			if(!(is_writable($this->_log_file)))
			{
				throw new Exception("LOGGER ERROR: Can't write to log file " . $this->_log_file, 1);
			}
		}
	}

	/**
	 * debug - Log at Debug level
	 * @param String message
	 * @return void
	 **/
	public function debug($message){
		$this->writeToLog("DEBUG", $message);
	}

	/**
	 * e - Log at Error level
	 * @param String message
	 * @author
	 **/
	public function error($message){
		$this->writeToLog("ERROR", $message);
	}

	/**
	 * w - Log at Warning level
	 * @param String message
	 * @author
	 **/
	public function warn($message){
		$this->writeToLog("WARNING", $message);
	}

	/**
	 * i - Log at Info level
	 * @param String message
	 * @return void
	 **/
	public function info($message){
		$this->writeToLog("INFO", $message);
	}

	/**
	 * writeToLog - writes out timestamped message to the log file
	 *
	 * @param String status - "INFO"/"DEBUG"/"ERROR"
	 * @param String message - The message you want to record
	 * @return void
	 **/
	private function writeToLog($status, $message) {
		$date = date(self::DATE_FORMAT);
		$msg = "$date: [$status] - $message" . PHP_EOL;
		file_put_contents($this->_log_file, $msg, FILE_APPEND);
	}
}
