<?php
require_once dirname(__FILE__) . '/logger.class.php';
require_once dirname(__FILE__) . '/config.inc.php';

class DbConn
{
	private $_environment = NOTIFY_ENVIRONMENT;

	private $_logger;
	public $notif;
	public $tats;
	public $redis;

	const DRIVER = 'mysql';
	/**
	 * Initialize database connection for notifications
	 * @param $env REQUIRED
	 */
	public function __construct()
	{
		$this->_logger = new Logger();
	}

	/**
	 * Initialize database connection for Notification
	 * @param $env
	 */
	public function connectNotifDb()
	{
		//DATABASE CONFIG
		if($this->_environment == 'beta')
		{
			global $betaNotDb;
			$dbtype   = $betaNotDb['dbtype'];
			$server   = $betaNotDb['server'];
			$dbname   = $betaNotDb['dbname'];
			$username = $betaNotDb['username'];
			$password = $betaNotDb['password'];
			$options = array(PDO::ATTR_PERSISTENT => true,
						PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
						PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
						PDO::ATTR_EMULATE_PREPARES => false,
			);
		}
		else if($this->_environment == 'live') {
			global $liveNotDb;
			$dbtype   = $liveNotDb['dbtype'];
			$server   = $liveNotDb['server'];
			$dbname   = $liveNotDb['dbname'];
			$username = $liveNotDb['username'];
			$password = $liveNotDb['password'];
			$options = array(PDO::ATTR_PERSISTENT => true,
						PDO::ATTR_ERRMODE => PDO::ERRMODE_SILENT,
						PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
						PDO::ATTR_EMULATE_PREPARES => false,
			);
		}
		else {
			$this->_logger->error("Invalid environment specified for database connection");
		}

		try {
			$this->notif = @new PDO("$dbtype:host=$server;dbname=$dbname", $username, $password, $options );
		}
		catch ( PDOException $e ) {
			$this->_logger->error("Could not connect to database! Error: " . $e->getMessage());
		}
	}
	
	/**
	 * Initialize database connection for TATS to read data using Slave
	 * @param $env
	 */
	public function connectTatsSlaveDb()
	{
		//TATS SLAVE DATABASE CONFIG
		global $slaveTatsDb;
		$dbtype   = $slaveTatsDb['dbtype'];
		$server   = $slaveTatsDb['server'];
		$dbname   = $slaveTatsDb['dbname'];
		$username = $slaveTatsDb['username'];
		$password = $slaveTatsDb['password'];
		$options = array(PDO::ATTR_PERSISTENT => true,
					PDO::ATTR_ERRMODE => PDO::ERRMODE_SILENT,
					PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
					PDO::ATTR_EMULATE_PREPARES => false,
		);

		try {
			$this->tats = @new PDO("$dbtype:host=$server;dbname=$dbname", $username, $password, $options );
		}
		catch ( PDOException $e ) {
			$this->_logger->error("Could not connect to database! Error: " . $e->getMessage());
		}
	}

	/**
	 * Initialize database connection for TATS to write - Master
	 * @param $env
	 */
	public function connectTatsMasterDb()
	{
		// TATS MASTER DATABASE CONFIG
		global $masterTatsDb;
		$dbtype   = $masterTatsDb['dbtype'];
		$server   = $masterTatsDb['server'];
		$dbname   = $masterTatsDb['dbname'];
		$username = $masterTatsDb['username'];
		$password = $masterTatsDb['password'];
		$options = array(PDO::ATTR_PERSISTENT => true,
					PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
					PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
					PDO::ATTR_EMULATE_PREPARES => false,
		);

		try {
			$this->tats = @new PDO("$dbtype:host=$server;dbname=$dbname", $username, $password, $options );
		}
		catch ( PDOException $e ) {
			$this->_logger->error("Could not connect to database! Error: " . $e->getMessage());
		}
	}

	/**
	 * Initialize redis connection
	 * @param $env
	 */
	public function connectRedis()
	{
		//CONFIG
		if($this->_environment == 'beta') {
			global $betaRedis;
			$aRedisConfig = $betaRedis;
		}
		else if($this->_environment == 'live') {
			global $liveRedis;
			$aRedisConfig = $liveRedis;
		}
		else {
			$this->_logger->error("Invalid environment specified for REDIS connection");
			$aRedisConfig = array();
		}

		$this->redis = Predis_Client::create($aRedisConfig);
		try {
			$this->redis->isConnected();
		}
		catch ( Exception $e ) {
			$this->_logger->error("Could not connect to redis! Error: " . $e->getMessage());
		}
	}

}
