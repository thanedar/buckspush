<?php
require_once dirname(__FILE__) . '/classes/processHistory.class.php';

require_once dirname(__FILE__) . '/utilities/logger.class.php';

$logger = new Logger();

// Run history update daemon, start threads
try {
	$oUpProcessor = new ProcessHistory(MAX_THREADS, IDLE_SLEEP_TIME); // Init
	$oUpProcessor->handle();                     // Run threads
}
catch( Exception $e ) {
	if ( $oUpProcessor==null ) {
		$sErr = $argv[0].': Daemon failed to start: '.$e->getMessage();
	}
	else {
		$sErr = $argv[0].': Daemon died: '.$e->getMessage();
	}
	$logger->error($sErr);
	die($sErr."\n");
}

