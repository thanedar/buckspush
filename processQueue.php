<?php
require_once dirname(__FILE__) . '/classes/processQueue.class.php';

require_once dirname(__FILE__) . '/utilities/logger.class.php';

$logger = new Logger();

// Run queue processor daemon, start threads
try {
	$oQProcessor = new ProcessQueue(MAX_THREADS, IDLE_SLEEP_TIME); // Init
	$oQProcessor->handle();                     // Run threads
}
catch( Exception $e ) {
	if ( $oQProcessor==null ) {
		$sErr = $argv[0].': Daemon failed to start: '.$e->getMessage();
	}
	else {
		$sErr = $argv[0].': Daemon died: '.$e->getMessage();
	}
	$logger->error($sErr);
	die($sErr."\n");
}

