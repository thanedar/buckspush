<?php
require_once dirname(__FILE__) . '/classes/processAPNFeedback.class.php';

require_once dirname(__FILE__) . '/utilities/logger.class.php';

$logger = new Logger();

// Run queue processor daemon, start threads
try {
	$oQProcessor = new ProcessQueue(1); // Init with one thread only
	$oQProcessor->handle();                     // Run threads
}
catch( Exception $e ) {
	if ( $oQProcessor==null ) {
		$sErr = $argv[0].': Daemon failed to start: '.$e->getMessage();
	}
	else {
		$sErr = $argv[0].': Daemon died: '.$e->getMessage();
	}
	$logger->error($sErr);
	die($sErr."\n");
}

