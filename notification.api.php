<?php

require_once dirname(__FILE__) . '/utilities/functions.inc.php';
require_once dirname(__FILE__) . '/utilities/logger.class.php';

require_once dirname(__FILE__) . '/classes/notification.class.php';
require_once dirname(__FILE__) . '/classes/campaign.class.php';
require_once dirname(__FILE__) . '/classes/message.class.php';
require_once dirname(__FILE__) . '/classes/appconfig.class.php';

$notification = new BP_Notification();
$logger = new Logger();

$aReturn = array('status' => 'ER', 'message' => '');

$logger->debug("In ".__FILE__ . " script");

// get input param values decoded to array
if(is_array($_POST)) {
	$sInputStr = file_get_contents('php://input');
	$aJsonInput = json_decode($sInputStr, true);
	
	if(!is_array($aJsonInput)) {
		if(isset($_POST['json'])) {
			$aJsonInput = json_decode($_POST['json'], true);
			$logger->debug("JSON request " . var_export($aJsonInput, true));
		}
		else if(count($_POST) > 0) {
			$logger->debug("POST request array " . var_export($_POST, true));
			$aJsonInput = $_POST;
		}
	}
}

if($aJsonInput) {
	$method = $aJsonInput['method'];
	unset($aJsonInput['method']);		// Or value interferes with PDO statements
	
	if($method == 'sendDeviceMessage') {
		// process for sending message for request received from TATS
		// all required params should already be passed in request as JSON object
		/*
		{
			‘campaign_id’ : 2, // Campaign ID configured in Notification DB
			‘message’ : ”Sample message to be sent to device”, // If not configured in campaign/ different message
			‘devices’: [
				{
					‘type’ : ‘Android’,
					‘device_id’ : ‘Abcd123456ABCD’, // GCM or APNS ID of the device
					‘timezone’ : ‘America/New_York’,
					‘lang’ : ‘English’,
					‘tracking_id’ : ‘abc123xyz’, // Optional unique tracking code for message
				},
				{
					‘type’ : ‘iOS’,
					‘device_id’ : ‘Abcd123456ABCD’, // GCM or APNS ID of the device
					‘locale’ : ‘US’,
					‘lang’ : ‘English’,
					‘tracking_id’ : ‘abc123xyz’, // Optional unique tracking code for message
				}
			]
		}
		*/
	
		$logger->debug("In device message block: " . var_export($aJsonInput, true));
	
		$campaign_id = $aJsonInput['campaign_id'] ? $aJsonInput['campaign_id'] : 0;
		$app_id = $aJsonInput['app_id'] ? $aJsonInput['app_id'] : 0;
		$message = $aJsonInput['message'];

		// initialize counters
		$nSuccess = $nFailure = 0;
		$fDevices = '';
		
		if(!empty($campaign_id)) {
			// check if we have a campaign for the given ID
			$campaign = new BP_Campaign($campaign_id);
			$bValidCampaign = $campaign->checkNotificationCampaign();
			if(!$bValidCampaign) {
				$aReturn['message'] = "Campaign configuration not found. Please check the given campaign ID";
				$logger->error($aReturn['message']);
			}
			else {
				$aInput = array('campaign_id' => $campaign_id);

				foreach($aJsonInput['devices'] as $device) {
					// merge device info into input and send to queue
					$aInput = array_merge($aInput, $device);
					$bRet = $notification->queueCampaignMessage($aInput);

					if($bRet) {
						$nSuccess++;
					}
					else {
						$nFailure++;
						$fDevices .= $device['device_id'] . ', ';
					}
				}

				$aReturn['status'] = 'OK';
				$aReturn['message'] = 'Messages queueing completed - Success ' . $nSuccess . PHP_EOL . 'Failed ' . $nFailure . PHP_EOL . 'Failed device IDs: ' . $fDevices ;
			}
		}
		else if(!empty($app_id) && !empty($message)) {
			// check if we have app with given ID configured
			$appconfig = new BP_AppConfig($app_id);
			$bValidConfig = $appconfig->checkAppConfig();
			if(!$bValidConfig) {
				$aReturn['message'] = "Package configuration not found. Please check the given package ID";
				$logger->error($aReturn['message']);
			}
			else {
				$aInput = array('message' => $message, 'app_id' => $app_id);

				foreach($aJsonInput['devices'] as $device) {
					// merge device info into input and send to queue
					$aInput = array_merge($aInput, $device);
					$bRet = $notification->queueCustomMessage($aInput);
					
					if($bRet) {
						$nSuccess++;
					}
					else {
						$nFailure++;
						$fDevices .= $device['device_id'] . ', ';
					}
				}

				$aReturn['status'] = 'OK';
				$aReturn['message'] = 'Messages queueing completed - Success ' . $nSuccess . PHP_EOL . 'Failed ' . $nFailure . PHP_EOL . 'Failed device IDs: ' . $fDevices ;
			}
		}
		else {
			$aReturn['message'] ="Both campaign ID and (app ID + message) cannot be empty in request";
			$logger->error($aReturn['message']);
		}
	}
	else if($method == 'sendProductMessage') {
		// process for sending message for request received from product
		// only product user ID is passed in request as JSON object
		// all required params should be gathered from TATS
		/*
		{
			‘campaign_id’ : 2, // Campaign ID configured in Notification DB
			‘message’ : ”Sample message to be sent to device”, // If not configured in campaign / different message, can be an array - for iOS this is the 'alert' array value
			‘user’: [
				{
					‘push_user_id’ : 123456, // TS_Push ID of the user
					‘product_user_id’ : 123456, // Product ID of the user
					‘tracking_id’ : ‘abc123xyz’, // Optional unique tracking code for message
				},
				{
					‘push_user_id’ : 123456, // TS_Push ID of the user
					‘product_user_id’ : 123456, // Product ID of the user
					‘tracking_id’ : ‘abc123xyz’, // Optional unique tracking code for message
				},
			]
		}
		*/
		$logger->debug("In product message block: " . var_export($aJsonInput, true));
	
		$campaign_id = !empty($aJsonInput['campaign_id']) ? $aJsonInput['campaign_id'] : 0;
		$app_id = !empty($aJsonInput['app_id']) ? $aJsonInput['app_id'] : 0;
		$message = $aJsonInput['message'];

		// initialize counters
		$nSuccess = $nFailure = 0;
		$fUsers = $sNotFound = '';
		
		if(!empty($campaign_id)) {
			// check if we have a campaign for the given ID
			$campaign = new BP_Campaign($campaign_id);
			$bValidCampaign = $campaign->checkNotificationCampaign();
			if(!$bValidCampaign) {
				$aReturn['message'] = "Campaign configuration not found. Please check the given campaign ID";
				$logger->error($aReturn['message']);
			}
			else {
				$aInput = array('campaign_id' => $campaign_id);

				$users = $aJsonInput['users'];
				unset($aJsonInput['users']);
				foreach($users as $user) {
					$bRet = false;
					$user['product_user_id'] = !empty($user['product_user_id']) ? $user['product_user_id'] : 0;
					
					// get info from TATS and merge into input and send to queue
					$aTatsUser = $notification->getTatsUserInfo($user['push_user_id'], $user['product_user_id'], $app_id, $campaign_id);
					if($aTatsUser) {
						$aInput = array_merge($aInput, $user, $aTatsUser);
						$bRet = $notification->queueCampaignMessage($aInput);
					}
					else {
						$sNotFound .= $user['id'] . ', ';
					}
					
					if($bRet) {
						$nSuccess++;
					}
					else {
						$nFailure++;
						$fUsers .= $user['id'] . ', ';
					}
				}

				$aReturn['status'] = 'OK';
				$aReturn['message'] = 'Messages queueing completed' . '    Success ' . $nSuccess . '    Failed ' . $nFailure;				
				if($nFailure > 0){
					$aReturn['message'] .= '    Failed IDs: ' . $fUsers  . '    Not Found in TATS: ' . $sNotFound ;
				}
			}
		}
		else if(!empty($app_id) && !empty($message)) {
			// check if we have app with given ID configured
			$appconfig = new BP_AppConfig($app_id);
			$bValidConfig = $appconfig->checkAppConfig();
			if(!$bValidConfig) {
				$aReturn['message'] = "Package configuration not found. Please check the given app ID";
				$logger->error($aReturn['message']);
			}
			else {
				$users = $aJsonInput['users'];
				unset($aJsonInput['users']);
				$aInput = $aJsonInput; 

				foreach($users as $user) {
					$bRet = false;
					$user['product_user_id'] = !empty($user['product_user_id']) ? $user['product_user_id'] : 0;
					$user['push_user_id'] = !empty($user['push_user_id']) ? $user['push_user_id'] : "";
					
					// get info from TATS and merge into input and send to queue
					$aTatsUser = $notification->getTatsUserInfo($user['push_user_id'], $user['product_user_id'], $app_id, $campaign_id);
					if($aTatsUser)
					{
						$logger->debug('In TatsUser with return ' . var_export($aTatsUser, true));
						$aInput = array_merge($aInput, $user, $aTatsUser);
						$bRet = $notification->queueCustomMessage($aInput);
					}
					else {
						$sNotFound .= $user['id'] . ', ';
					}
					
					if($bRet) {
						$nSuccess++;
					}
					else {
						$nFailure++;
						$fUsers .= $user['push_user_id'] . ', ';
					}
				}

				$aReturn['status'] = 'OK';
				$aReturn['message'] = 'Messages queueing completed' . '    Success ' . $nSuccess . '    Failed ' . $nFailure;				
				if($nFailure > 0){
					$aReturn['message'] .= '    Failed IDs: ' . $fUsers  . '    Not Found in TATS: ' . $sNotFound ;
				}
			}
		}
		else {
			$aReturn['message'] ="Both campaign ID and (app ID + message) cannot be empty in request";
			$logger->error($aReturn['message']);
		}
	}
	else if($method == 'addNotificationCampaign') {
		$logger->debug("In add campaign block: " . var_export($aJsonInput, true));
	
		if(empty($aJsonInput['app_id'])) {
			$aReturn['message'] = "Cannot proceed - Empty app ID";
			$logger->error($aReturn['message']);
		}
		else {
			$logger->debug("Add new campaign");
			$campaign = new BP_Campaign($campaign_id);
			$bRet = $campaign->addNotificationCampaign($aJsonInput);

			if($bRet) {
				$aReturn['status'] = 'OK';
				$aReturn['message'] = "Campaign added successfully";
				$logger->debug($aReturn['message']);
			}
			else {
				$aReturn['message'] = "Campaign could NOT be added";
				$logger->error($aReturn['message']);
			}
		}
	}
	else if($method == 'updateNotificationCampaign') {
		$logger->debug("In update campaign block: " . var_export($aJsonInput, true));
	
		if(empty($aJsonInput['app_id']) || empty($aJsonInput['campaign_id'])) {
			$aReturn['message'] = "Cannot proceed - Empty app or campaign ID";
			$logger->error($aReturn['message']);
		}
		else {
			$campaign = new BP_Campaign($campaign_id);
			$bValidCampaign = $campaign->checkNotificationCampaign();
			if(!$bValidCampaign) {
				$bRet = $campaign->updateNotificationCampaign($aJsonInput);
				if($bRet) {
					$aReturn['status'] = 'OK';
					$aReturn['message'] = "Campaign updated successfully";
					$logger->debug($aReturn['message']);
				}
				else {
					$aReturn['message'] = "Campaign could NOT be updated";
					$logger->error($aReturn['message']);
				}
			}
			else {
				$aReturn['message'] = "Campaign configuration not found. Please check given campaign ID";
				$logger->error($aReturn['message']);
			}
		}
	}
	else if($method == 'addAppConfig') {
		$logger->debug("In add app config block: " . var_export($aJsonInput, true));
	
		$appconfig = new BP_AppConfig($app_id);
		$bRet = $appconfig->addAppConfig($aJsonInput);

		if($bRet) {
			$aReturn['status'] = 'OK';
			$aReturn['message'] = "Config added successfully";
			$logger->debug($aReturn['message']);
		}
		else {
			$aReturn['message'] = "Config could NOT be added";
			$logger->error($aReturn['message']);
		}
	}
	else if($method == 'updateAppConfig') {
		$logger->debug("In update app config block: " . var_export($aJsonInput, true));
	
		if(empty($aJsonInput['app_id'])) {
			$aReturn['message'] = "Cannot proceed - Empty app ID";
			$logger->error($aReturn['message']);
		}
		else {
			$appconfig = new BP_AppConfig($app_id);
			$bRet = $appconfig->updateAppConfig($aJsonInput);
			if($bRet) {
				$aReturn['status'] = 'OK';
				$aReturn['message'] = "Config updated successfully";
				$logger->debug($aReturn['message']);
			}
			else {
				$aReturn['message'] = "Config could NOT be updated";
				$logger->error($aReturn['message']);
			}
		}
	}
	else if($method == 'checkAppConfig') {
		$app_id = $aJsonInput['app_id'];
		$appconfig = new BP_AppConfig($app_id);
		$bValidConfig = $appconfig->checkAppConfig();
		//echo ('App config check returned ' . var_export($bConfigResult, true));
		$aReturn['status'] = $bValidConfig;
		
		if(!$bValidConfig) {	
			$aReturn['message'] = 'No matching app found for ID given - ' . $app_id;
		}
	}
	else if($method == 'checkNotificationCampaign') {
		$campaign_id = $aJsonInput['campaign_id'];
		//echo ('campaign check returned ' . var_export($bCampaignResult, true));
		$campaign = new BP_Campaign($campaign_id);
		$bValidCampaign = $campaign->checkNotificationCampaign();
		$aReturn['status'] = $bCampaignResult;
		if(!$bValidCampaign) {
			$aReturn['message'] = 'No matching campaign found for ID given - ' . $campaign_id;
		}
	}
	else if($method == 'getNotificationCampaign') {
		$campaign_id = $aJsonInput['campaign_id'];
		$campaign = new BP_Campaign($campaign_id);
		$aCampaignReturn = $campaign->getNotificationCampaign();
		$logger->debug('campaign returned ' . var_export($aCampaignReturn, true));
		
		if($aCampaignReturn) {
			$aReturn['status'] = 'OK';
			$aReturn['campaign'] = $aCampaignReturn;
		}
		else{
			$aReturn['message'] = 'No matching campaign found for ID given - ' . $campaign_id;
		}
	}
	else if($method == 'getAllCampaignMessages') {
		$message = new BP_Message($aJsonInput['campaign_id']);
		$aMessageReturn = $message->getAllCampaignMessages();
		if($aMessageReturn) {
			$aReturn['status'] = 'OK';
			$aReturn['campaign_message'] = $aMessageReturn;
		}
		else {
			$aReturn['message'] = 'No matching campaign message found for given parameters';
		}
	}
	else if($method == 'getMessageByLanguageName') {
		$message = new BP_Message($aJsonInput['campaign_id']);
		$aMessageReturn = $message->getCmpgnMessageByLangName($aJsonInput['language_name']);
		if($aMessageReturn) {
			$aReturn['status'] = 'OK';
			$aReturn['campaign_message'] = $aMessageReturn;
		}
		else {
			$aReturn['message'] = 'No matching campaign message found for given parameters';
		}
	}
	else if($method == 'getMessageByLanguageCode') {
		$message = new BP_Message($aJsonInput['campaign_id']);
		$aMessageReturn = $message->getCmpgnMessageByLangCode($aJsonInput['language_code']);
		if($aMessageReturn) {
			$aReturn['status'] = 'OK';
			$aReturn['campaign_message'] = $aMessageReturn;
		}
		else {
			$aReturn['message'] = 'No matching campaign message found for given parameters';
		}
	}
	else if($method == 'getMessageByLanguageId') {
		$message = new BP_Message($aJsonInput['campaign_id']);
		$aMessageReturn = $message->getCmpgnMessageByLangId($aJsonInput['language_id']);
		if($aMessageReturn) {
			$aReturn['status'] = 'OK';
			$aReturn['campaign_message'] = $aMessageReturn;
		}
		else {
			$aReturn['message'] = 'No matching campaign message found for given parameters';
		}
	}
	else if($method == 'getTatsUserInfo') {
		$aTatsReturn = $notification->getTatsUserInfo($aJsonInput['user_id'], $aJsonInput['app_id'], $aJsonInput['campaign_id']);
		if($aTatsReturn) {
			$aReturn['status'] = 'OK';
			$aReturn['tats_info'] = $aTatsReturn;
		}
		else {
			$aReturn['message'] = 'No matching user info found for given parameters';
		}
	}
	else if($method == 'sendPushMessage') {
		$aReturn = $notification->processQueue();
		$logger->debug('sendPushMessage returned ' . var_export($aReturn, true));
	}
	else if($method == 'testRedisConn') {
		$aReturn = $notification->testRedisConn();
		$logger->debug('testRedisConn returned ' . var_export($aReturn, true));
	}
	else if($method == 'getNotificationHistory') {
		if(empty($aJsonInput['push_user_id'])) {
			$aReturn['message'] = "Cannot proceed - Empty push user ID";
			$logger->error($aReturn['message']);
		}
		else {
			$aHistReturn = $notification->getNotificationHistory($aJsonInput['push_user_id'], $aJsonInput['tracking_id'], $aJsonInput['history_id']);
			$logger->debug('getNotificationHistory returned ' . var_export($aHistReturn, true));
			if($aHistReturn) {
				$aReturn['status'] = 'OK';
				$aReturn['history'] = $aHistReturn;
			}
			else {
				$aReturn['message'] = 'No matching message found for given parameters';
			}
		}
	}
}
else {
	$aReturn['message'] ="No input parameters found!!!";
	$logger->error("Cannot proceed - Empty/Invalid request");
	$logger->error($aReturn['message']);
}

die(json_encode($aReturn));
